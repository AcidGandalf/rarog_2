// Copyright (c) <2020> Vii
#pragma once

#include <arctic/engine/easy.h>
#include <arctic/engine/unicode.h>

struct String32 {
  std::vector<Ui32> data;

  Ui32 *Data() {
    if (data.size()) {
      return &data[0];
    } else {
      return nullptr;
    }
  }

  String32() {
  }

  String32(const Ui32* p, const Ui32* p1) {
    data.resize(p1 - p + 1);
    if (data.size() > 1) {
      memcpy(data.data(), p, (p1 - p) * sizeof(Ui32));
    }
    data[data.size() - 1] = 0;
  }

  String32(const char* psz_utf8_text) {
    Ui64 length = 0;
    Utf32Reader reader;
    reader.Reset(psz_utf8_text);
    Ui32 ch;
    do {
      ch = reader.ReadOne();
      length++;
    } while(ch);
    reader.Reset(psz_utf8_text);
    data.resize(length);
    Ui64 pos = 0;
    do {
      ch = reader.ReadOne();
      data[pos] = ch;
      pos++;
    } while(ch);
  }

  virtual ~String32() {
  }
};

namespace std {
  template <>
  struct hash<String32> {
    size_t operator()(String32 const& x) const {
      size_t h = 37;
      for (const Ui32 i: x.data) {
         h = (h * 54059) ^ (static_cast<size_t>(i) * 76963);
      }
      return h; // or return h % 86969;
    }
  };
};

inline bool operator==(const String32& lhs, const String32& rhs) {
  return lhs.data.size() == rhs.data.size() &&
    memcmp(lhs.data.data(), rhs.data.data(), rhs.data.size() * sizeof(Ui32)) == 0;
}

inline bool operator!=(const String32& lhs, const String32& rhs) {
  return !(lhs == rhs);
}

