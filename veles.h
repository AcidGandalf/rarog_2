#ifndef VELES_H_
#define VELES_H_

#include <arctic/engine/easy.h>

using namespace arctic;

struct Veles64;
inline Ui64 StubOpIn(Veles64* veles, Ui64 port) {
  return 0;
}

inline void StubOpOut(Veles64* veles, Ui64 data, Ui64 port) {
  return;
}

struct Veles64 {
  static constexpr Si64 kRamSizeWords = (1ull << 21);
  static constexpr Si64 kCoreCount = 8;
  static constexpr Ui64 kOpcodeMask = 7;
  static constexpr Ui64 kABMask = (1ull << 21) - 1;
  static constexpr Ui64 kAShift = 3;
  static constexpr Ui64 kBShift = 3 + 21;
  static constexpr Ui64 kCShift = 3 + 21 + 21;

  enum Opcode {
    kOpSub = 0,
    kOpNand = 1,
    kOpRol = 2,
    kOpJg = 3,
    kOpFadd = 4,
    kOpCas = 5,
    kOpIn = 6,
    kOpOut = 7
  };

  struct CoreState {
    Ui64 ip = 0;
    Ui64 c1 = 0;
  };

  Ui64 Time = 0;
  CoreState Core[kCoreCount];
  Ui64 Ram[kRamSizeWords] = {0};

  std::function<Ui64(Veles64*, Ui64)> OpInput = StubOpIn;
  std::function<void(Veles64*, Ui64, Ui64)> OpOutput = StubOpOut;
};

void Step(Veles64* v);
Ui64 MakeOp(Veles64::Opcode opcode, Ui64 a, Ui64 b, Ui64 c);

#endif  // VELES_H_
