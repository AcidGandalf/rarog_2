// Copyright (c) <2019> Vii

#define _USE_MATH_DEFINES

#include "engine/easy.h"
#include "engine/unicode.h"
#include "engine/vec2d.h"
#include "engine/mat22d.h"

#include "veles.h"
#include "veles_asm.h"

#include <cmath>
#include <unordered_map>
#include <iomanip>


using namespace arctic;  // NOLINT


#define TXT_WID 120
#define TXT_HEI 49


Si32 g_level = 1;
const char* g_mission_text = "";

Si32 g_text_x = 0;
Si32 g_text_y = 0;
Rgba g_text_fore_color = Rgba((Ui32)0xffffffff);
Rgba g_text_back_color = Rgba((Ui32)0x000000ff);

double g_prev_time;
double g_cur_time;

Sound g_boom;
Sound g_shot;
Sound g_tap;

Sound g_music;

Si32 g_space_music_idx = 0;
std::vector<Sound> g_space_music;
Si64 g_frame = 0;

Sound g_asteroids_voice;
std::unordered_map<char32_t, Sprite> g_ch;
std::vector<Vec2D> g_ship_points;
std::vector<Vec2D> g_directions;

Si64 g_tick = 0;
bool g_is_up = false;
bool g_is_down = false;
bool g_is_left = false;
bool g_is_right = false;
bool g_is_shoot = false;

const double kBulletMass = 200;
const double kBulletExitSpeed = 5900;
const double kBulletExitTime = 0.004746f / 2.f;
const double kExhaustSpeed = 66000.f;
const double kEngineFuelConsumption = 192.f;

Ui64 ExecuteOpIn(Veles64 *veles, Ui64 port);
void ExecuteOpOut(Veles64 *veles, Ui64 data, Ui64 port);

struct Ship {
  Vec2D Position;
  Vec2D Velocity;
  double Angle;
  double W;
  Si64 Bullets;

  double LeftEngineM;
  double RightEngineM;
  double FuelM;
  double GunM;
  double OneBulletM;
  double ComputerM;
  double FrameM;

  double Mass;

  bool FireLeft;
  bool FireRight;
  bool FireRailgun;

  Si64 selected_body;
  Si64 cached_body;
  Ui64 body_cache[4];
  Ui64 body_cache_available;
  Ui64 self_cache[7];
  Si64 self_cache_available;

  std::deque<std::string> veles_log;
  bool is_last_veles_log_record_complete = true;
  std::deque<Ui64> to_earth;
  Si64 items_sent;
  Veles64 *cpu = nullptr;
  Veles64Asm *compiler = nullptr;

  void Reset() {
    Position = Vec2D(0.f, 0.f);
    Velocity = Vec2D(0.f, 0.f);
    Angle = 0.f;
    W = 0.f;
    Bullets = 196;

    LeftEngineM = 33000.f;
    RightEngineM = 33000.f;
    FuelM = 200000.f;
    GunM = 20000.f;
    OneBulletM = 200.f;
    ComputerM = 5000.f;
    FrameM = 10000.f;

    UpdateMass();

    FireLeft = false;
    FireRight = false;
    FireRailgun = false;

    selected_body = 0;
    cached_body = -1;
    memset(body_cache, 0, sizeof(Ui64)*4);
    body_cache_available = 0;
    memset(self_cache, 0, sizeof(Ui64)*7);
    self_cache_available = 0;

    veles_log.clear();
    items_sent = 0;
    cpu = new Veles64();
    cpu->OpInput = ExecuteOpIn;
    cpu->OpOutput = ExecuteOpOut;
    {
      std::vector<Ui8> source = ReadFile("data/code.txt", true);
      compiler = new Veles64Asm();
      compiler->Compile(source);
      std::stringstream str;
      if (compiler->first_error.is_set) {
        str << "Compiler error,";
        if (compiler->first_error.line_id >= 0) {
          str << " line: " << (compiler->first_error.line_id + 1);
        }
        if (compiler->first_error.conflicting_line_id >= 0) {
          str << " conflicting line: " << (compiler->first_error.conflicting_line_id + 1);
        }
        str << " message: " << compiler->first_error.description;
        veles_log.push_back(str.str());
        Log(str.str().c_str());
      } else {
        str << "Compiler compiled the code";
        veles_log.push_back(str.str());
        Log(str.str().c_str());
        WriteFile("data/compiled.v64",
          reinterpret_cast<const Ui8*>(compiler->compiled.data()),
          compiler->compiled.size() * sizeof(Ui64));

        memcpy(cpu->Ram, compiler->compiled.data(), compiler->compiled.size() * sizeof(Ui64));
      }

    }
  }

  inline bool HasCachedBodyInfo(Si64 body_n) {
    if (body_n == cached_body) {
      if (body_cache_available > 0) {
        return true;
      }
    }
    return false;
  }

  inline Si64 ReadCachedBodyInfo() {
    if (body_cache_available > 0) {
      --body_cache_available;
      return body_cache[body_cache_available];
    }
    return 0;
  }

  inline void CacheBodyInfo(Si64 body_n, Si64 fwd, Si64 right, Si64 m, Si64 r) {
    cached_body = body_n;
    body_cache_available = 4;
    body_cache[0] = fwd;
    body_cache[1] = right;
    body_cache[2] = m;
    body_cache[3] = r;
    return;
  }

  inline bool HasCachedSelf() {
    return self_cache_available > 0;
  }

  inline Si64 ReadCachedSelf() {
    if (self_cache_available > 0) {
      --self_cache_available;
      return self_cache[self_cache_available];
    }
    return 0;
  }

  inline void CacheSelfInfo(Si64 x, Si64 y, Si64 vx, Si64 vy, Si64 fwdx, Si64 fwdy, Si64 w) {
    self_cache_available = 7;
    self_cache[0] = x;
    self_cache[1] = y;
    self_cache[2] = vx;
    self_cache[3] = vy;
    self_cache[4] = fwdx;
    self_cache[5] = fwdy;
    self_cache[6] = w;
    return;
  }

  inline void UpdateMass() {
    Mass = LeftEngineM +
      RightEngineM +
      FuelM +
      GunM +
      Bullets * OneBulletM +
      ComputerM +
      FrameM;
  }

  inline void SpendFuel(double dt) {
    double to_burn = std::min(FuelM, dt * kEngineFuelConsumption);
    FuelM -= to_burn;
    Mass -= to_burn;
  }
};

struct Bullet {
  Vec2D PrevPosition;
  Vec2D Position;
  Vec2D Velocity;
  double TimeToLive;
};

struct Particle {
  Vec2D PrevPosition;
  Vec2D Position;
  Vec2D Velocity;
  Rgba Color;
  double TimeToLive;
};

struct Rock {
  std::vector<Vec2D> Points;
  Vec2D Position;
  Vec2D Velocity;
  double Angle;
  double W;
  double Mass;
  double BoundingR;
  double Surface2d;

  double TmpDistanceSq;

  void FromAbsolutePoints() {
    Angle = 0.f;
    Position = Vec2D(0.f, 0.f);
    BoundingR = 0.f;
    if (Points.size()) {
      for (size_t i = 0; i < Points.size(); ++i) {
        Position += Points[i];
      }
      Position /= (double)Points.size();
      for (size_t i = 0; i < Points.size(); ++i) {
        Points[i] -= Position;
        double r = Length(Points[i]);
        if (r > BoundingR) {
          BoundingR = r;
        }
      }
    }
    CalculateSurface2d();
  }

  void CalculateSurface2d() {
    Surface2d = 0.f;
    for (size_t i = 0; i < Points.size(); ++i) {
      Vec2D a(0.f, 0.f);
      Vec2D b(Points[i]);
      Vec2D c(Points[(i + 1) % Points.size()]);
      Surface2d += 0.5f * abs((b.x-a.x)*(c.y-a.y) - (c.x-a.x)*(b.y-a.y));
    }
  }
};

double g_zoom = 1.f;
Vec2D g_camera;

Ship g_ship;
std::vector<Bullet> g_bullets;
size_t g_oldest_particle_idx = 0;
std::vector<Particle> g_particles;
std::vector<Rock> g_rocks;
std::vector<Rock> g_safe_rocks;
std::vector<size_t> g_bullet_idx_to_remove;

struct Symbol {
  Ui32 code;
  Rgba fore_color;
  Rgba back_color;
};

Symbol g_text_buf[TXT_WID * TXT_HEI];


void cls() {
  for (Si32 i = 0; i < TXT_WID * TXT_HEI; ++i) {
    Symbol &s = g_text_buf[i];
    s.code = (Ui32)' ';
    s.fore_color.rgba = 0xffffffff;
    s.back_color.rgba = 0;
  }
  g_text_fore_color.rgba = 0xffffffff;
  g_text_back_color.rgba = 0;
  g_text_x = 0;
  g_text_y = 0;
}

std::vector<Rgba> named_colors {
  Rgba(255, 255, 255, 0), Rgba(255, 255, 255, 0)
};

#define COLOR_WHITE u8"\001"

// control character is 0x25d1 or u8"◑"
const char * print(const char *text) {
  Utf32Reader reader;
  reader.Reset((const Ui8 *)text);
  for (Ui32 ch = reader.ReadOne(); ch != 0; ch = reader.ReadOne()) {
    if (ch == 0x25d1) {
      Ui32 color_code = reader.ReadOne();
      g_text_fore_color = named_colors[color_code];
    } else if (ch == '\n') {
      g_text_x = 0;
      g_text_y++;
    } else {
      Si32 pos = (g_text_y * TXT_WID + g_text_x) % (TXT_WID * TXT_HEI);
      Symbol &s = g_text_buf[pos];
      s.code = ch;
      s.fore_color = g_text_fore_color;
      s.back_color = g_text_back_color;
      g_text_x++;
    }
  }
  return (const char *)reader.p;
}

// p - point
// v - start point of segment
// w - end point of segment
double DistToSegmentSq (Vec2D p, Vec2D v, Vec2D w) {
  double l2 = LengthSquared(v - w);
  if (l2 == 0.f) {
    return LengthSquared(p - v);
  }
  double t = ((p[0] - v[0]) * (w[0] - v[0]) + (p[1] - v[1]) * (w[1] - v[1])) / l2;
  t = std::max(0.0, std::min(1.0, t));
  return LengthSquared(p - Vec2D(v[0] + t * (w[0] - v[0]), v[1] + t * (w[1] - v[1])));
}

double DistToLineSq(Vec2D p, Vec2D v, Vec2D w) {
  double l2 = LengthSquared(v - w);
  if (l2 == 0.f) {
    return LengthSquared(p - v);
  }
  double t = ((p[0] - v[0]) * (w[0] - v[0]) + (p[1] - v[1]) * (w[1] - v[1])) / l2;
  return LengthSquared(p - Vec2D(v[0] + t * (w[0] - v[0]), v[1] + t * (w[1] - v[1])));
}


// Given three colinear points p, q, r, the function checks if
// point q lies on line segment 'pr'
bool onSegment(Vec2D p, Vec2D q, Vec2D r) {
  if (q.x <= std::max(p.x, r.x) && q.x >= std::min(p.x, r.x) &&
      q.y <= std::max(p.y, r.y) && q.y >= std::min(p.y, r.y)) {
    return true;
  }

  return false;
}

// To find orientation of ordered triplet (p, q, r).
// The function returns following values
// 0 --> p, q and r are colinear
// 1 --> Clockwise
// 2 --> Counterclockwise
int orientation(Vec2D p, Vec2D q, Vec2D r) {
  // See https://www.geeksforgeeks.org/orientation-3-ordered-points/
  // for details of below formula.
  int val = int((q.y - p.y) * (r.x - q.x) -
  (q.x - p.x) * (r.y - q.y));

  if (val == 0) {
    return 0;  // colinear
  }

  return (val > 0)? 1: 2; // clock or counterclock wise
}

// The main function that returns true if line segment 'p1q1'
// and 'p2q2' intersect.
// returns intersection point where particle traveling from p1 to q1 hits p2q2
bool LineLineIntersect(Vec2D p1, Vec2D q1, Vec2D p2, Vec2D q2, Vec2D *ip) {
  // Find the four orientations needed for general and
  // special cases
  int o1 = orientation(p1, q1, p2);
  int o2 = orientation(p1, q1, q2);
  int o3 = orientation(p2, q2, p1);
  int o4 = orientation(p2, q2, q1);

  // General case
  if (o1 != o2 && o3 != o4) {
    double d1 = sqrt(DistToLineSq(p1, p2, q2));
    double d2 = sqrt(DistToLineSq(q1, p2, q2));
    *ip = p1 + (q1 - p1) * (d1 / (d1 + d2));
    return true;
  }

  // Special Cases
  // p1, q1 and p2 are colinear and p2 lies on segment p1q1
  if (o1 == 0 && onSegment(p1, p2, q1)) {
    *ip = p2;
    return true;
  }

  // p1, q1 and q2 are colinear and q2 lies on segment p1q1
  if (o2 == 0 && onSegment(p1, q2, q1)) {
    *ip = q2;
    return true;
  }

  // p2, q2 and p1 are colinear and p1 lies on segment p2q2
  if (o3 == 0 && onSegment(p2, p1, q2)) {
    *ip = p1;
    return true;
  }

  // p2, q2 and q1 are colinear and q1 lies on segment p2q2
  if (o4 == 0 && onSegment(p2, q1, q2)) {
    *ip = q1;
    return true;
  }

  return false; // Doesn't fall in any of the above cases
}


void SpawnParticle(Vec2D position, Vec2D velocity, double time_to_live) {
  Particle *p = nullptr;
  if (g_particles.size() < 20000) {
    g_particles.emplace_back();
    p = &g_particles.back();
  } else {
    p = &g_particles[g_oldest_particle_idx];
    g_oldest_particle_idx++;
    if (g_oldest_particle_idx >= g_particles.size()) {
      g_oldest_particle_idx = 0;
    }
  }
  p->PrevPosition = position;
  p->Position = position;
  p->Velocity = velocity;
  p->TimeToLive = time_to_live;
}

void UpdateMusic(bool is_space) {
  if (!g_music.IsPlaying()) {
    if (is_space) {
      g_space_music_idx = (g_space_music_idx + 1) % g_space_music.size();
      g_music = g_space_music[g_space_music_idx];
    }
    g_music.Play();
  }
}

void DrawShip() {
  Mat22D rotation = Rotation(g_ship.Angle);
  for (size_t i = 0; i + 1 < g_ship_points.size(); i += 2) {
    DrawLine(Vec2Si32((g_ship.Position + rotation * g_ship_points[i]) * g_zoom - g_camera),
             Vec2Si32((g_ship.Position + rotation * g_ship_points[i+1]) * g_zoom - g_camera),
             Rgba(240, 145, 25));
  }
}

void DrawBullets() {
  for (size_t i = 0; i < g_bullets.size(); ++i) {
    Bullet &b = g_bullets[i];
    DrawLine(Vec2Si32(b.PrevPosition * g_zoom - g_camera), Vec2Si32(b.Position * g_zoom - g_camera),
             Rgba(200, 125, 25), Rgba(240, 145, 25));
  }
}

void DrawParticles() {
  for (size_t i = 0; i < g_particles.size(); ++i) {
    Particle &p = g_particles[i];
    DrawLine(Vec2Si32(p.PrevPosition * g_zoom - g_camera), Vec2Si32(p.Position * g_zoom - g_camera),
             Rgba(200, 125, 25), Rgba(240, 145, 25));
  }
}

void DrawRocks() {
  Vec2D ulc(-ScreenSize().x/2.f, ScreenSize().y/2.f);
  Vec2D urc(ScreenSize().x/2.f, ScreenSize().y/2.f);
  Vec2D llc(-ScreenSize().x/2.f, -ScreenSize().y/2.f);
  Vec2D lrc(ScreenSize().x/2.f, -ScreenSize().y/2.f);

  Vec2D los0 = Vec2D(0.f, 0.f);

  for (size_t r = 0; r < g_rocks.size(); ++r) {
    Rock &rock = g_rocks[r];

    Vec2D scr_pos = rock.Position * g_zoom - g_camera;
    if (scr_pos.x + rock.BoundingR < 0.f
        || scr_pos.x - rock.BoundingR > ScreenSize().x
        || scr_pos.y + rock.BoundingR < 0.f
        || scr_pos.y - rock.BoundingR > ScreenSize().y) {
      // draw a marker

      Vec2D los1 = scr_pos - Vec2D(ScreenSize()) / 2.f;

      bool is_intersecting = false;
      Vec2D ip(0.f, 0.f);
      is_intersecting = is_intersecting || LineLineIntersect(los0, los1, ulc, urc, &ip);
      is_intersecting = is_intersecting || LineLineIntersect(los0, los1, urc, lrc, &ip);
      is_intersecting = is_intersecting || LineLineIntersect(los0, los1, llc, lrc, &ip);
      is_intersecting = is_intersecting || LineLineIntersect(los0, los1, ulc, llc, &ip);

      if (is_intersecting) {
        Vec2Si32 pos = Vec2Si32(ip * 0.98f) + ScreenSize()/2;
        DrawCircle(pos, 2, Rgba(240, 145, 25));

        Vec2D rel_vel = rock.Velocity - g_ship.Velocity;
        double rel_spd = Length(rel_vel);
        if (rel_spd > 1.f) {
          Vec2D rel_vel_dir = rel_vel / rel_spd;
          double len = 5.f;
          if (rel_spd > 100.f) {
            len = 6.f;
          }
          if (rel_spd > 400.f) {
            len = 7.f;
          }
          Vec2Si32 vec = Vec2Si32(rel_vel_dir * len);
          DrawLine(pos, pos + vec, Rgba(240, 145, 25));
        }

        char dist[128];
        snprintf(dist, sizeof(dist), " %d ", Si32(Length(g_ship.Position - rock.Position)));
        Si32 dl = (Si32)strlen(dist);
        Vec2Si32 pos2 = Vec2Si32(ip * 0.97f) + ScreenSize()/2;

        g_text_x = Clamp(Si32(pos2.x) * TXT_WID / ScreenSize().x, 0, TXT_WID - 1 - dl);
        g_text_y = Clamp(Si32(ScreenSize(). y - pos2.y) * TXT_HEI / ScreenSize().y, 0, TXT_HEI - 1);
        print(dist);
      }
    }
    Mat22D rotation = Rotation(rock.Angle);
    if (rock.Points.size() > 1) {
      // DrawCircle(Vec2Si32(rock.Position * g_zoom), rock.BoundingR * g_zoom, Rgba(24,14,2));
      Vec2D prev = rock.Points.back();
      for (size_t i = 0; i < rock.Points.size(); ++i) {
        DrawLine(Vec2Si32((rock.Position + rotation * prev) * g_zoom - g_camera),
                 Vec2Si32((rock.Position + rotation * rock.Points[i]) * g_zoom - g_camera),
                 Rgba(240, 145, 25));
        prev = rock.Points[i];
      }
    }
  }
  for (size_t r = 0; r < g_safe_rocks.size(); ++r) {
    Rock &rock = g_safe_rocks[r];
    Mat22D rotation = Rotation(rock.Angle);
    if (rock.Points.size() > 1) {
      // DrawCircle(Vec2Si32(rock.Position * g_zoom), rock.BoundingR * g_zoom, Rgba(24,14,2));
      Vec2D prev = rock.Points.back();
      for (size_t i = 0; i < rock.Points.size(); ++i) {
        DrawLine(Vec2Si32((rock.Position + rotation * prev) * g_zoom - g_camera),
                 Vec2Si32((rock.Position + rotation * rock.Points[i]) * g_zoom - g_camera),
                 Rgba(120, 75, 15));
        prev = rock.Points[i];
      }

    }
  }
}

void CreateRock(double r1, double r2) {
  g_rocks.emplace_back();
  Rock &rock = g_rocks.back();
  Si32 point_count = Random32(5, 16);
  rock.Points.resize(point_count);
  double angles[16];
  bool is_good = false;
  while (!is_good) {
    is_good = true;
    for (Si32 i = 0; i < point_count; ++i) {
      angles[i] = double(Random(0, 65536) * M_PI * 2.f / 65536.f);
    }
    std::sort(angles, angles + point_count);
    double prev_angle = angles[point_count - 1] - double(M_PI * 2.f);
    for (Si32 i = 0; i < point_count; ++i) {
      double d = angles[i] - prev_angle;
      if (d > M_PI / 1.1f) {
        is_good = false;
      }
      prev_angle = angles[i];
    }
  }
  double max_r = 0;
  for (Si32 i = 0; i < point_count; ++i) {
    double r = Random((Si64)(r1 * 65536.f), (Si64)(r2 * 65536.f)) / 65536.f;
    max_r = std::max(max_r, r);
    double a = angles[i];

    Mat22D rotation = Rotation(a);
    Vec2D forward = rotation * Vec2D(1.f, 0.f);
    rock.Points[i] = forward * r;
  }
  rock.Position = Vec2D(100.f, 100.f);
  rock.Velocity = Vec2D(0.f, 0.f);
  rock.Mass = 4.f / 3.f * double(M_PI) * (r1 + r2) * (r1 + r2) * (r1 + r2) * 0.5f * 0.5f * 0.5f * 5000.f;
  rock.Angle = 0.f;
  rock.W = 0.f;
  rock.BoundingR = max_r;
  rock.CalculateSurface2d();
}


double CanonicAngle(double angle) {
  double canonic = std::fmodf(std::fmodf(angle, (double)M_PI * 2.0f) + (double)M_PI * 2.0f, (double)M_PI * 2.0f);
  if (canonic > (double)M_PI) {
    canonic -= double(2.f * M_PI);
  }
  return canonic;
}

Vec2D ToPolar(Vec2D vec) {
  double len = Length(vec);
  double dir = 0.f;
  if (len > 0.f) {
    Vec2D norm = vec / len;
    dir = CanonicAngle(std::atan2(norm.y, norm.x));
  }
  return Vec2D(dir, len);
}

void SplitRock(size_t rock_idx, Vec2D cp) {
  Rock r = g_rocks[rock_idx];
  g_rocks[rock_idx] = g_rocks[g_rocks.size() - 1];
  g_rocks.pop_back();

  Vec2D ep = cp - r.Position;
  Vec2D pp = ToPolar(ep);
  Mat22D rock_rotation = Rotation(r.Angle);
  for (size_t i = 0; i < r.Points.size(); ++i) {
    r.Points[i] = rock_rotation * r.Points[i];
  }

  size_t min_idx = 0;
  for (size_t i = 1; i < r.Points.size(); ++i) {
    if (ToPolar(r.Points[i]).x < ToPolar(r.Points[min_idx]).x) {
      min_idx = i;
    }
  }
  size_t first_idx = min_idx;
  for (size_t i = 0; i < r.Points.size(); ++i) {
    size_t idx = (min_idx + i) % r.Points.size();
    if (pp.x < ToPolar(r.Points[idx]).x) {
      first_idx = idx;
      break;
    }
  }

  size_t begin_rock_idx = g_rocks.size();
  size_t rock_count = Random32(2, (Si32)r.Points.size());
  size_t cur_idx = first_idx;
  double total_r_cube = 0.f;
  for (size_t n = 0; n < rock_count; ++n) {
    g_rocks.emplace_back();
    Rock &nr = g_rocks.back();

    nr.Points.reserve(r.Points.size());
    nr.Points.push_back(r.Position);
    if (n == 0) {
      nr.Points.push_back(cp);
    }

    size_t max_step = r.Points.size() + first_idx - cur_idx - (rock_count - n - 1);
    size_t next_idx = cur_idx + (size_t)Random32(1, (Si32)max_step);
    if (n == rock_count - 1) {
      next_idx = first_idx + r.Points.size();
    }
    for (; cur_idx < next_idx; ++cur_idx) {
      nr.Points.push_back(r.Position + r.Points[cur_idx % r.Points.size()]);
    }
    --cur_idx;
    if (n == rock_count - 1) {
      nr.Points.push_back(cp);
    }

    nr.FromAbsolutePoints();
    //total_r_cube += nr.BoundingR * nr.BoundingR ;
    total_r_cube += nr.Surface2d;
  }
  for (size_t n = begin_rock_idx; n < g_rocks.size(); ++n) {
    Rock &nr = g_rocks[n];
    //double r_cube = nr.BoundingR * nr.BoundingR ;
    double r_cube = nr.Surface2d;
    nr.Mass = r.Mass * (r_cube / total_r_cube) * 0.75f;
    nr.W = r.W;
    nr.Velocity = r.Velocity + Normalize(nr.Position - cp) * (double)Random32(1, 65536) / 65536.f * 20.f;
  }
  for (size_t n = begin_rock_idx; n < g_rocks.size(); ++n) {
    if (g_rocks[n].Mass < 2500000.f || g_rocks[n].BoundingR < 5.f) {
      g_safe_rocks.push_back(g_rocks[n]);
      g_rocks[n] = g_rocks.back();
      g_rocks.pop_back();
      n--;
    }
  }
}

void DrawTextOverlay() {

  Sprite backbuffer = GetEngine()->GetBackbuffer();
  Sprite full = g_ch[0x2588]; //u8"█"
  for (Si32 i = 0; i < TXT_WID * TXT_HEI; ++i) {
    Symbol &s = g_text_buf[i];
    Si32 x = i % TXT_WID * 8;
    Si32 y = ScreenSize().y - i / TXT_WID * 11 - 11;
    full.Draw(backbuffer,
              x, y, kDrawBlendingModeColorize, kFilterNearest, s.back_color);
    g_ch[s.code].Draw(backbuffer,
                      x, y, kDrawBlendingModeColorize, kFilterNearest,
                      Rgba(s.fore_color.r + Random32(-15, 0),
                           s.fore_color.g + Random32(-15, 0),
                           s.fore_color.b + Random32(-15, 0),
                           s.fore_color.a)
                      );
  }
}

void InitLevel();

void ShowGameComplete() {
  ShowFrame();
  while (!IsKeyDownward(kKeyEscape) && !IsKeyDownward(kKeyEnter)) {
    g_frame++;
    if (g_frame % 100 == 0) {
      UpdateMusic(false);
    }

    Clear();
    cls();
    g_text_fore_color = Rgba(240, 145, 25);
    print(u8R"LIT(
                                              П О З Д Р А В Л Е Н И Е


    Человечество спасено на этот раз. Все празднуют победу, но Войскам Стратегической Обороны Земли расслабляться
  нельзя, космос огромен и таит новые угрозы.


                                Н А Ж М И   В В О Д   Ч Т О Б Ы   П О В Т О Р И Т Ь
          )LIT");

    DrawTextOverlay();

    ShowFrame();
  }
  g_level = 1;
  InitLevel();
}


void InitLevel() {
  g_ship.Reset();

  g_bullets.clear();
  g_oldest_particle_idx = 0;
  g_particles.clear();
  g_rocks.clear();
  g_safe_rocks.clear();
  g_bullet_idx_to_remove.clear();

  switch(g_level) {
    case 1:
      CreateRock(5.f, 5.f);
      g_rocks.back().W = Random(-65536, 65536) * 4.f / 65536.f;
      g_rocks.back().Position = Vec2D(100.f, 000.f);
      CreateRock(5.f, 5.f);
      g_rocks.back().W = Random(-65536, 65536) * 4.f / 65536.f;
      g_rocks.back().Position = Vec2D(000.f, 100.f);
      CreateRock(5.f, 5.f);
      g_rocks.back().W = Random(-65536, 65536) * 4.f / 65536.f;
      g_rocks.back().Position = Vec2D(-200.f, 100.f);
      g_mission_text = u8"  М И С С И Я   1\n  Цель: уничтожить 3 опасных астероида прицельным огнем.";
      break;
    case 2:
      CreateRock(5.f, 7.f);
      g_rocks.back().W = Random(-65536, 65536) * 4.f / 65536.f;
      g_rocks.back().Position = Vec2D(500.f, 200.f);
      CreateRock(5.f, 7.f);
      g_rocks.back().W = Random(-65536, 65536) * 4.f / 65536.f;
      g_rocks.back().Position = Vec2D(800.f, 300.f);
      CreateRock(5.f, 7.f);
      g_rocks.back().W = Random(-65536, 65536) * 4.f / 65536.f;
      g_rocks.back().Position = Vec2D(1100.f, 500.f);
      CreateRock(2.f, 7.f);
      g_rocks.back().W = Random(-65536, 65536) * 4.f / 65536.f;
      g_rocks.back().Position = Vec2D(1400.f, 400.f);
      g_mission_text = u8"  М И С С И Я   2\n  Цель: Подлететь к отмеченным маркерами астероидам и уничтожить их.";
      break;
    case 3:
      CreateRock(5.f, 10.f);
      g_rocks.back().W = Random(-65536, 65536) * 4.f / 65536.f;
      CreateRock(5.f, 10.f);
      g_rocks.back().W = Random(-65536, 65536) * 4.f / 65536.f;
      g_rocks.back().Position = Vec2D(-500.f, -200.f);
      CreateRock(5.f, 10.f);
      g_rocks.back().W = Random(-65536, 65536) * 4.f / 65536.f;
      g_rocks.back().Position = Vec2D(-540.f, -230.f);
      CreateRock(5.f, 10.f);
      g_rocks.back().W = Random(-65536, 65536) * 4.f / 65536.f;
      g_rocks.back().Position = Vec2D(-700.f, 250.f);
      CreateRock(5.f, 12.f);
      g_rocks.back().W = Random(-65536, 65536) * 4.f / 65536.f;
      g_rocks.back().Position = Vec2D(-770.f, 320.f);
      CreateRock(5.f, 10.f);
      g_rocks.back().W = Random(-65536, 65536) * 4.f / 65536.f;
      g_rocks.back().Position = Vec2D(-720.f, 300.f);
      g_mission_text = u8"  М И С С И Я   3\n  Цель: Уничтожить опасные астероиды и крупные осколки в случае образования.";
      break;
    case 4:
      CreateRock(10.f, 12.f);
      g_rocks.back().W = Random(-65536, 65536) * 4.f / 65536.f;
      g_rocks.back().Velocity = Vec2D(15.f, 3.f);
      CreateRock(10.f, 12.f);
      g_rocks.back().W = Random(-65536, 65536) * 4.f / 65536.f;
      g_rocks.back().Position = Vec2D(-70.f, 50.f);
      g_rocks.back().Velocity = Vec2D(10.f, 10.f);
      CreateRock(5.f, 12.f);
      g_rocks.back().W = Random(-65536, 65536) * 4.f / 65536.f;
      g_rocks.back().Position = Vec2D(-100.f, 50.f);
      g_rocks.back().Velocity = Vec2D(7.f, 8.f);
      CreateRock(5.f, 12.f);
      g_rocks.back().W = Random(-65536, 65536) * 4.f / 65536.f;
      g_rocks.back().Position = Vec2D(-100.f, -50.f);
      g_rocks.back().Velocity = Vec2D(6.f, 9.f);
      CreateRock(5.f, 12.f);
      g_rocks.back().W = Random(-65536, 65536) * 4.f / 65536.f;
      g_rocks.back().Position = Vec2D(-170.f, -150.f);
      g_rocks.back().Velocity = Vec2D(5.f, 11.f);
      CreateRock(5.f, 12.f);
      g_rocks.back().W = Random(-65536, 65536) * 4.f / 65536.f;
      g_rocks.back().Position = Vec2D(-270.f, -100.f);
      g_rocks.back().Velocity = Vec2D(7.f, 11.f);

      g_mission_text = u8"  М И С С И Я   4\n  Цель: Уничтожить опасные астероиды и крупные осколки.";
      break;
    case 5:
      CreateRock(12.f, 13.f);
      g_rocks.back().W = Random(-65536, 65536) * 4.f / 65536.f;
      CreateRock(10.f, 16.f);
      g_rocks.back().Position = Vec2D(200.f, 140.f);
      g_rocks.back().W = Random(-65536, 65536) * 4.f / 65536.f;
      g_rocks.back().Velocity = Vec2D(-5.f, 3.f);
      CreateRock(12.f, 20.f);
      g_rocks.back().Position = Vec2D(150.f, 240.f);
      g_rocks.back().Velocity = Vec2D(-3.f, 2.f);
      g_rocks.back().W = Random(-65536, 65536) * 4.f / 65536.f;
      g_mission_text = u8"  М И С С И Я   5\n  Цель: Уничтожить огромные астероиды и крупные осколки.";
      break;
    case 6:
      CreateRock(20.f, 25.f);
      g_rocks.back().W = Random(-65536, 65536) * 4.f / 65536.f;
      g_rocks.back().Position = Vec2D(400.f, 250.f);
      g_rocks.back().Velocity = Vec2D(-30.f, -10.f);
      g_mission_text = u8"  М И С С И Я   6\n  Цель: Уничтожить гигантский астероид и крупные осколки.";
      break;
    case 7:
      CreateRock(25.f, 35.f);
      g_rocks.back().W = Random(-65536, 65536) * 4.f / 65536.f;
      g_mission_text = u8"  М И С С И Я   7\n  Цель: Спасти человечество.";
      break;
    case 8:
      ShowGameComplete();
      break;
  }
}


void Initialize() {
  g_space_music.emplace_back();
  g_space_music.back().Load("data/Neon Transit (With Guitar).ogg", false);
  g_space_music_idx = 0;

  g_music = g_space_music.back();
  g_music.Play();
  g_asteroids_voice.Load("data/asteroids.ogg", false);
  g_asteroids_voice.Play(3.0);

  g_space_music.emplace_back();
  g_space_music.back().Load("data/Epic Fall.ogg", false);

  g_boom.Load("data/boom.ogg", true);
  g_shot.Load("data/shot.ogg", true);
  g_tap.Load("data/tap.ogg", true);

  ResizeScreen(960, 540);

  g_directions.resize(2048);
  for (Si32 i = 0; i < (Si32)g_directions.size(); ++i) {
    double angle = double(i * M_PI * 2.0 / g_directions.size());
    g_directions[i] = Vec2D(cos(angle), sin(angle));
  }

  // Load font
  std::vector<std::string> vec = {
    u8"П", u8"Я", u8"Р", u8"С", u8"Т", u8"У", u8"Ж", u8"В", u8"Ь", u8"Ы", u8"З", u8"Ш", u8"Э", u8"Щ", u8"Ч", u8"Ъ",
    u8"Ю", u8"А", u8"Б", u8"Ц", u8"Д", u8"Е", u8"Ф", u8"Г", u8"Х", u8"И", u8"Й", u8"К", u8"Л", u8"М", u8"Н", u8"О",
    u8"п", u8"я", u8"р", u8"с", u8"т", u8"у", u8"ж", u8"в", u8"ь", u8"ы", u8"з", u8"ш", u8"э", u8"щ", u8"ч", u8"ъ",
    u8"ю", u8"а", u8"б", u8"ц", u8"д", u8"е", u8"ф", u8"г", u8"х", u8"и", u8"й", u8"к", u8"л", u8"м", u8"н", u8"о",
    u8"p", u8"q", u8"r", u8"s", u8"t", u8"u", u8"v", u8"w", u8"x", u8"y", u8"z", u8"{", u8"|", u8"}", u8" ", u8" ",
    u8"`", u8"a", u8"b", u8"c", u8"d", u8"e", u8"f", u8"g", u8"h", u8"i", u8"j", u8"k", u8"l", u8"m", u8"n", u8"o",
    u8"P", u8"Q", u8"R", u8"S", u8"T", u8"U", u8"V", u8"W", u8"X", u8"Y", u8"Z", u8"[", u8"\\", u8"]", u8" ", u8"_",
    u8"@", u8"A", u8"B", u8"C", u8"D", u8"E", u8"F", u8"G", u8"H", u8"I", u8"J", u8"K", u8"L", u8"M", u8"N", u8"O",
    u8"0", u8"1", u8"2", u8"3", u8"4", u8"5", u8"6", u8"7", u8"8", u8"9", u8":", u8";", u8"<", u8"=", u8">", u8"?",
    u8" ", u8"!", u8"\"", u8"#", u8"$", u8"%", u8"&", u8"'", u8"(", u8")", u8"*", u8"+", u8",", u8"-", u8".", u8"/"};
  Sprite font;
  font.Load("data/font.tga");
  for (Si32 idx = 0; idx < (Si32)vec.size(); ++idx) {
    Si32 x = idx % 16;
    Si32 y = idx / 16;
    const Ui8* p = (const Ui8*)vec[idx].data();
    Utf32Reader reader;
    reader.Reset(p);
    Ui32 u = reader.ReadOne();
    Sprite tmp;
    tmp.Reference(font, x * 8, y * 11, 8, 11);
    g_ch[u].Clone(tmp);
    g_ch[u].UpdateOpaqueSpans();
  }

  // Load ship model
  std::vector<double> raw_ship = {
    -4.0, 4.0, -4.0, 3.0, -4.0, 3.0, 4.0, 3.0, 4.0, 4.0, -4.0, 4.0,
    -4.0, 0.0, -8.0, 0.0, -8.0, 0.0, -6.0, 9.0, -6.0, 9.0, -4.0, 9.0,
    -4.0, 9.0, -4.0, 0.0, 4.0, 9.0, 4.0, 0.0, 4.0, 0.0, 8.0, 0.0,
    8.0, 0.0, 6.0, 9.0, 6.0, 9.0, 4.0, 9.0, -2.0, 4.0, -2.0, 8.0,
    -2.0, 8.0, 2.0, 8.0, 2.0, 8.0, 2.0, 4.0, 3.0, 8.0, -3.0, 8.0,
    -3.0, 8.0, -3.0, 11.0, -3.0, 11.0, 3.0, 11.0, 3.0, 11.0, 3.0, 8.0,
    0.5, 11.0, 0.5, 25.0, 0.5, 25.0, -0.5, 25.0, -0.5, 25.0, -0.5, 11.0};
  for (size_t i = 0; i + 1 < raw_ship.size(); i += 2) {
    g_ship_points.emplace_back((double)raw_ship[i+1] - 7.0f, (double)-raw_ship[i]);
  }

  InitLevel();
}


void UpdateTime() {
  g_prev_time = g_cur_time;
  g_cur_time = Time();
}




void PlayTitleScreen() {
  ShowFrame();
  while (!IsKeyDownward(kKeyEscape) && !IsKeyDownward(kKeySpace)) {
    UpdateTime();
    g_frame++;
    if (g_frame % 100 == 0) {
      UpdateMusic(true);
    }


    Clear();
    cls();
g_text_fore_color = Rgba(240, 145, 25);
print(u8R"LIT(
                                                     R A R O G - 1

    К земле приближается несколько гигантских небесных тел диаметром более 200 метров. Чтобы уничтожить такой объект,
  необходим ядерный удар с энерговыделением более мегатонны в тротиловом эквиваленте. После взрыва часть астероида
  превратится в газ и капли жидкости, а часть распадется на осколки. Осколки размером менее 10 метров для Земли уже не
  будут представлять никакой угрозы.

    Астероиды были замечены за год до столкновения. Международная организация "Войска Стратегической Обороны Земли"
  в рекордно короткие сроки провела более 20 запусков тяжелых ракет Протон-М и на орбите был собран уникальный корабль
  Rarog-1 со стартовой массой 341 т.

    Rarog-1, приводимый в движение двумя облегченными ядерными ракетными двигателями на гомогенном растворе солей
  ядерного топлива Svarog-L, вооружен новейшим рельсотроном Yarilo-M, способным придать скорость 5900 м/с каждому из
  196 тактических ядерных снарядов мощностью 210 килотонн. Так как ни человек, ни интегральные микросхемы не выдержат
  жесткой радиации на борту, управление осуществляет специально спроектированная ЭВМ с центральным процессором на
  вакуумных лампах Veles64.

    Rarog-1 должен уничтожить опасные астероиды и, в случае образования, крупные осколки. Перехват состоится чуть
  дальше луны, на расстоянии около 400 000 километров от земли, что оставит возможность для второй попытки уничтожения
  опасных астероидов ядерными ракетами с земли.

    Судьба всего человечества зависит от успеха корабля войск Стратегической Обороны Земли Rarog-1.


                                                  У П Р А В Л Е Н И Е

    Клавиши со стрелками - вращение и ускорение корабля Rarog-1
    Пробел - выстрел из рельсотрона Yarilo-M
    Z - переключение масштаба
    ESC - выход из игры


                    Н А Ж М И   П Р О Б Е Л   Д Л Я   З А П У С К А   К О Р А Б Л Я   R A R O G - 1



                                          Н А Д   И Г Р О Й   Р А Б О Т А Л И

  Игровой движок Arctic Engine:                                Игровой код: Вий [NML]
  Copyright (c) 2016 - 2019 Huldra                             Голос за кадром: Alexandr Zhelanov
  Copyright (c) 2017 Romain Sylvian                            Музыка: Alexandr Zhelanov
  Copyright (c) 2013 - 2017 Martin Mitas                               https://soundcloud.com/alexandr-zhelanov
  Copyright (c) 2015 - 2016 Inigo Quilez                               licensed under CC BY 3.0
  Copyright (c) 2009 - 2010 Barry Schwartz
  Copyright (c) 2018 Vitaliy Manushkin <agri@akamo.info>
  Copyright (c) 2013 - 2014 RAD Game Tools and Valve Software
  Copyright (c) 2010 - 2014 Rich Geldreich and Tenacious Software LLC
  Copyright (c) 2017 Mikulas Florek
      )LIT");

    DrawTextOverlay();


    ShowFrame();
  }
}

void Show15Intro() {
  ShowFrame();
  while (!IsKeyDownward(kKeyEscape) && !IsKeyDownward(kKeyEnter)) {
    Clear();
    cls();
    g_text_fore_color = Rgba(240, 145, 25);
    print(u8R"LIT(
                       В О С С Т А Н О В Л Е Н И Е   А М О Р А Л И   П Е Р С О Н А Л А   Ц У П


    Приказом ВСОЗ номер 286 утверждена обязательная процедура поднятия боевого духа и возбуждения желания спасать
  человечество.




                                                  У П Р А В Л Е Н И Е

    Манипулятор МЫШЬ - выбор передвигаемого элемента специального изображения
    Пробел - сигнал о завершении восстановления аморали (отключен до восстановления изображения)
    ESC - выход из игры




                             Н А Ж М И   В В О Д   Д Л Я   Н А Ч А Л А   П Р О Ц Е Д У Р Ы
          )LIT");

    DrawTextOverlay();

    ShowFrame();
  }
}


void UpdateKeys() {
  g_is_up = (IsKeyDown(kKeyUp));
  g_is_down = (IsKeyDown(kKeyDown));
  g_is_left = (IsKeyDown(kKeyLeft));
  g_is_right = (IsKeyDown(kKeyRight));
  g_is_shoot = (IsKeyDownward(kKeySpace));
}

std::vector<Si32> g_part_idx;

bool TryMove(Si32 cx, Si32 cy, Si32 w, Si32 h) {
  Si32 m = w * h - 1;
  if (g_part_idx[cy * w + cx] != m) {
    if (cx > 0 && g_part_idx[cy * w + cx - 1] == m) {
      std::swap(g_part_idx[cy * w + cx - 1], g_part_idx[cy * w + cx]);
      return true;
    } else if (cx < w - 1 && g_part_idx[cy * w + cx + 1] == m) {
      std::swap(g_part_idx[cy * w + cx + 1], g_part_idx[cy * w + cx]);
      return true;
    } else if (cy > 0 && g_part_idx[(cy - 1) * w + cx] == m) {
      std::swap(g_part_idx[(cy - 1) * w + cx], g_part_idx[cy * w + cx]);
      return true;
    } else if (cy < h - 1 && g_part_idx[(cy + 1) * w + cx] == m) {
      std::swap(g_part_idx[(cy + 1) * w + cx], g_part_idx[cy * w + cx]);
      return true;
    }
  }
  return false;
}


void UpdateCamera() {
  g_camera = g_ship.Position * g_zoom - Vec2D(ScreenSize()) / 2.f;
}


Ui64 ExecuteOpIn(Veles64 *veles, Ui64 port) {
  switch (port) {
    case 0: // earth com
      break;
    case 4:
      // 4 - прочитать собственную позицию, скорость, ориентацию, угловую скорость
      if (!g_ship.HasCachedSelf()) {
        Si64 x = static_cast<Si64>(g_ship.Position.x * 1048576.f);
        Si64 y = static_cast<Si64>(g_ship.Position.y * 1048576.f);
        Si64 vx = static_cast<Si64>(g_ship.Velocity.x * 1048576.f);
        Si64 vy = static_cast<Si64>(g_ship.Velocity.y * 1048576.f);
        Vec2D forward_vec = Rotation(g_ship.Angle) * Vec2D(1048576.f, 0.f);
        Si64 fwdx = static_cast<Si64>(forward_vec.x);
        Si64 fwdy = static_cast<Si64>(forward_vec.y);
        Si64 w = static_cast<Si64>(g_ship.W * (1048576.f/ (M_PI * 2.f)));
        // x: DW ? ; позиция корабля вдоль глобальной оси x в 1/1048576 долях метра
        // y: DW ? ; позиция корабля вдоль глобальной оси y в 1/1048576 долях метра
        // vx: DW ? ; скорость корабля вдоль глобальной оси x в 1/1048576 долях м/с
        // vy: DW ? ; скорость корабля вдоль глобальной оси y в 1/1048576 долях м/с
        // fwdx: DW ? ; координата х вектора длиной 1048576 направленного по направлению «вперед»
        // fwdy: DW ? ; координата y вектора длиной 1048576 направленного по направлению «вперед»
        // w: DW ? ; угловая скорость корабля в 1/1048576 долях оборота/с против часовой стрелки
        g_ship.CacheSelfInfo(x, y, vx, vy, fwdx, fwdy, w);
      }
      return g_ship.ReadCachedBodyInfo();
    case 5:
      // 5 - прочитать позицию, массу и радиус выбранного тела
      if (!g_ship.HasCachedBodyInfo(g_ship.selected_body)) {
        // Select nth rock
        if (g_ship.selected_body >= g_rocks.size() || g_ship.selected_body < 0) {
          // If there is no rock with such index, cache zeroes
          g_ship.CacheBodyInfo(g_ship.selected_body, 0, 0, 0, 0);
        } else {
          // Sort rocks based on distance
          std::vector<Rock*> sorted_rocks;
          sorted_rocks.resize(g_rocks.size());
          for (size_t i = 0; i < g_rocks.size(); ++i) {
            Rock &r = g_rocks[i];
            r.TmpDistanceSq = LengthSquared(r.Position - g_ship.Position);
            sorted_rocks[i] = &g_rocks[i];
          }
          std::sort(sorted_rocks.begin(), sorted_rocks.end(),
            [](const Rock* const& a, const Rock* const& b) {return a->TmpDistanceSq < b->TmpDistanceSq;});
          Rock &rock = *sorted_rocks[g_ship.selected_body];
          Vec2D to_rock = rock.Position - g_ship.Position;

          Vec2D normal_right = Vec2D(1.f, 0.f);
          Vec2D normal_down = Vec2D(0.f, -1.f);
          Mat22D rotation = Rotation(g_ship.Angle);
          Vec2D forward_vec = rotation * normal_right;
          Vec2D right_vec = rotation * normal_down;

          Si64 fwd = static_cast<Si64>(Dot(to_rock, forward_vec) * 1048576.f);
          Si64 right = static_cast<Si64>(Dot(to_rock, right_vec) * 1048576.f);
          Si64 m = static_cast<Si64>(rock.Mass * 1048576.f);
          Si64 r = static_cast<Si64>(rock.BoundingR * 1048576.f);
          // Else cache the actual data
          g_ship.CacheBodyInfo(g_ship.selected_body, fwd, right, m, r);
          // fwd: DW ? ; расстояние вдоль вектора «вперед» в 1/1048576 долях метра
          // right: DW ? ; расстояние вдоль вектора «направо» в 1/1048576 долях метра
          // m: DW ? ; масса в 1/1048576 долях килограмма
          // r: DW ? ; радиус в 1/1048576 долях метра
        }
      }
      return g_ship.ReadCachedBodyInfo();
  }
  return 0;
}

void ExecuteOpOut(Veles64 *veles, Ui64 data, Ui64 port) {
  switch (port) {
    case 0: // earth com
      g_ship.to_earth.push_back(data);
      break;
    case 1:
      // 1 - левый двигатель вкл/выкл (не 0/0)
      g_ship.FireLeft = (data == 0 ? false : true);
      break;
    case 2:
      // 2 - правый двигатель вкл/выкл (не 0/0)
      g_ship.FireRight = (data == 0 ? false : true);
      break;
    case 3:
      // 3 - рельсотрон залп (не 0)
      g_ship.FireRailgun = (data == 0 ? false : true);
      break;
    case 5:
      // 5 - выбрать тело (сортированы по расстоянию, 0 - ближайшее)
      g_ship.selected_body = data;
      break;
  }
  return;
}

static void SendMessagesToEarth() {
  Si64 full_items_sent = g_ship.items_sent;
  if (!g_ship.is_last_veles_log_record_complete) {
    g_ship.veles_log.pop_back();
  }
  g_ship.is_last_veles_log_record_complete = true;
  for (Si64 i = g_ship.items_sent; i < g_ship.to_earth.size(); ++i) {
    std::stringstream str;
    bool is_message = g_ship.to_earth[i] < g_ship.compiler->message_dict.size();
    if (!is_message) {
      str << static_cast<Si64>(g_ship.to_earth[i]);
    } else {
      String32 &msg = g_ship.compiler->message_dict[g_ship.to_earth[i]];
      Ui32 *p0 = msg.Data();
      Ui32 *p1 = p0;
      while (*p0 != 0) {
        while (*p1 != 0 && *p1 != '%' && *p1 != '$' && *p1 != '#') {
          ++p1;
        }
        if (*p1 == 0) {
          str << Utf32ToUtf8(p0).c_str();
        } else {
          if (p0 != p1) {
            String32 text(p0, p1);
            str << Utf32ToUtf8(text.Data()).c_str();
          }
          p0 = p1;
          ++i;
          if (i >= g_ship.to_earth.size()) {
            str << Utf32ToUtf8(p0).c_str();
            g_ship.is_last_veles_log_record_complete = false;
            while (*p1 != 0) {
              ++p1;
            }
            p0 = p1;
          } else {
            if (*p1 == '$') {
              str << static_cast<Si64>(g_ship.to_earth[i]);
            } else if (*p1 == '#') {
              str << std::hex << g_ship.to_earth[i] << "h";
            } else if (*p1 == '%') {
              str << (static_cast<Si64>(g_ship.to_earth[i]) / 1048576);
              str << ".";
              str << std::setfill('0') << std::setw(6)
              << Si64(double(abs(static_cast<Si64>(g_ship.to_earth[i]) % 1048576))/1048576.f*1000000.f);
            }
            p1++;
            p0 = p1;
          }
        }
        p0 = p1;
      }
    }
    g_ship.veles_log.push_back(str.str());
    if (g_ship.is_last_veles_log_record_complete) {
      full_items_sent = i + 1;
    }
  }
  g_ship.items_sent = full_items_sent;
}

void PlayGame() {
  UpdateCamera();

  g_tick = 0;

  ShowFrame();

  double time_to_end_mission = 3.0;

  double tick_duration = 0.0;
  double total_duration = 0.0;
  double total_ticks = 0.0;

  while (!IsKeyDownward(kKeyEscape)) {
    g_frame++;
    if (g_frame % 100 == 0) {
      UpdateMusic(true);
    }
    UpdateTime();
    double dt = g_cur_time - g_prev_time;

    if (g_rocks.size() == 0) {
      time_to_end_mission -= dt;
      if (time_to_end_mission < 0.0) {
        time_to_end_mission = 3.0;
        if (IsKeyDownward(kKeyEscape)) {
          break;
        }
        g_level++;
        InitLevel();
        UpdateTime();
        UpdateTime();
        dt = g_cur_time - g_prev_time;
      }
    } else if (g_ship.Bullets == 0 && g_bullets.size() == 0) {
      time_to_end_mission -= dt;
      if (time_to_end_mission < 0.0) {
        time_to_end_mission = 3.0;
        InitLevel();
        UpdateTime();
        UpdateTime();
        dt = g_cur_time - g_prev_time;
      }
    }

    UpdateKeys();

    if (IsKeyDownward(kKeyZ)) {
      g_zoom /= 2.f;
      if (g_zoom < 0.1f) {
        g_zoom = 5.f;
      }
    }

    constexpr double fdt = 1/16.f/1024.f;
    if (dt > 0.1f) {
      dt = 0.1f;
    }
    Si64 dt_ticks = static_cast<Si64>(dt / fdt + 0.5);
    Si64 t2_ticks = g_tick + dt_ticks;
    const Vec2D normal_right = Vec2D(1.f, 0.f);
    const Vec2D normal_down = Vec2D(0.f, -1.f);
    bool did_shoot = false;

    for (size_t i = 0; i < g_bullets.size(); ++i) {
      Bullet &b = g_bullets[i];
      b.PrevPosition = b.Position;
    }
    for (size_t i = 0; i < g_particles.size(); ++i) {
      Particle &p = g_particles[i];
      p.PrevPosition = p.Position;
    }


    double t0 = Time();
    total_ticks += (t2_ticks - g_tick);
    while (g_tick < t2_ticks) {
      Step(g_ship.cpu);
      g_tick++;

      g_ship.Position += g_ship.Velocity * fdt;
      g_ship.Angle += g_ship.W * fdt;

      bool fire_left = false;
      bool fire_right = false;
      if (g_is_left) {
        fire_right = true;
      }
      if (g_is_right) {
        fire_left = true;
      }
      if (g_is_up) {
        if (!g_is_left && !g_is_right) {
          fire_left = true;
          fire_right = true;
        }
      }
      fire_left = fire_left || g_ship.FireLeft;
      fire_right = fire_right || g_ship.FireRight;
      bool shoot = false;
      if (g_is_shoot || g_ship.FireRailgun) {
        if (!did_shoot) {
          shoot = true;
          did_shoot = true;
        }
      }

      if (fire_left || fire_right || shoot) {
        Mat22D rotation = Rotation(g_ship.Angle);
        Vec2D forward = rotation * normal_right;
        if (shoot) {
          g_ship.FireRailgun = false;
          if (g_ship.Bullets) {
            g_ship.Bullets--;
            g_ship.UpdateMass();
            // bullet attributes
            // a = 1243214
            g_bullets.emplace_back();
            g_bullets.back().PrevPosition = g_ship.Position + forward * kBulletExitSpeed * kBulletExitTime;
            g_bullets.back().Position = g_bullets.back().PrevPosition;
            g_bullets.back().Velocity = g_ship.Velocity + forward * kBulletExitSpeed;
            g_bullets.back().TimeToLive = 1.f;

            Vec2D bullet_p = kBulletMass * (forward * kBulletExitSpeed);
            Vec2D ship_dv = bullet_p / -g_ship.Mass;
            g_ship.Velocity += ship_dv;

            g_shot.Play();
          }
        }
        if (fire_left && fire_right) {
          if (g_ship.FuelM > 0.f) {
            g_ship.SpendFuel(fdt * 2.f);
            const double inv_mass = 1.f / g_ship.Mass;
            g_ship.Velocity += forward * (kEngineFuelConsumption * fdt * 2.f * kExhaustSpeed * inv_mass);
          }
        } else if (fire_right || fire_left) {
          if (g_ship.FuelM > 0.f) {
            g_ship.SpendFuel(fdt);
            if (fire_right) {
              const double inv_mass = 1.f / g_ship.Mass;
              g_ship.W += 0.1f * kEngineFuelConsumption * fdt * kExhaustSpeed * inv_mass;
              g_ship.Velocity += forward * (kEngineFuelConsumption * fdt * kExhaustSpeed * inv_mass);
            }
            if (fire_left) {
              const double inv_mass = 1.f / g_ship.Mass;
              g_ship.W -= 0.1f * kEngineFuelConsumption * fdt * kExhaustSpeed * inv_mass;
              g_ship.Velocity += forward * (kEngineFuelConsumption * fdt * kExhaustSpeed * inv_mass);
            }
          }
        }

        if ((g_tick & 0x3f) == 0) {
          Vec2D right = rotation * normal_down;
          if (fire_left) {
            SpawnParticle(g_ship.Position - forward * 7.f - right * 6.f,
                          g_ship.Velocity - forward * 100.f + Vec2D((double)Random32(-20, 20), (double)Random32(-20, 20)),
                          Random32(5000, 10000) / 1000.f);
          }
          if (fire_right) {
            SpawnParticle(g_ship.Position - forward * 7.f + right * 6.f,
                          g_ship.Velocity - forward * 100.f + Vec2D((double)Random32(-20, 20), (double)Random32(-20, 20)),
                          Random32(5000, 10000) / 1000.f);
          }
        }
      }

      const size_t rocks_size = g_rocks.size();
      for (size_t rock_idx = 0; rock_idx < rocks_size; ++rock_idx) {
        Rock &r = g_rocks[rock_idx];
        r.Position += r.Velocity * fdt;
        r.Angle = CanonicAngle(r.Angle + r.W * fdt);
      }

      // Intersect bullets vs rocks
      const size_t bullets_size = g_bullets.size();
      for (size_t bullet_idx = 0; bullet_idx < bullets_size; ++bullet_idx) {
        Bullet &b = g_bullets[bullet_idx];
        b.Position += b.Velocity * fdt;
        double min_travel_sq = std::numeric_limits<double>::max();
        size_t min_travel_rock_idx = g_rocks.size();
        Vec2D min_travel_cp;
        const Vec2D full_bf = b.Position - b.PrevPosition;
        if (LengthSquared(full_bf) > 0.f) {
          const size_t rocks_size = g_rocks.size();
          for (size_t rock_idx = 0; rock_idx < rocks_size; ++rock_idx) {
            const Rock &r = g_rocks[rock_idx];
            double dsq = DistToSegmentSq(r.Position, b.PrevPosition, b.Position);
            if (dsq <= r.BoundingR * r.BoundingR) {
              // test vs each line
              const Mat22D rock_rotation = Rotation(r.Angle);
              Vec2D prev = r.Points.back();
              for (size_t point_idx = 0; point_idx < r.Points.size(); ++point_idx) {
                Vec2D cp;
                bool is_intersecting = LineLineIntersect(b.PrevPosition, b.Position,
                    r.Position + rock_rotation * prev,
                    r.Position + rock_rotation * r.Points[point_idx],
                    &cp);
                if (is_intersecting) {
                  //cp = (r.Position + rock_rotation * prev +
                  //r.Position + rock_rotation * r.Points[point_idx]) / 2.f;
                  Vec2D bf = cp - b.PrevPosition;
                  double travel_sq = LengthSquared(bf);
                  if (travel_sq < min_travel_sq) {
                    min_travel_sq = travel_sq;
                    min_travel_rock_idx = rock_idx;
                    min_travel_cp = cp;
                  }

                }
                prev = r.Points[point_idx];
              }
            }
          }
        }
        if (min_travel_rock_idx < g_rocks.size()) {
          g_bullet_idx_to_remove.push_back(bullet_idx);
          // boom!
          b.Position = b.PrevPosition + (b.Position - b.PrevPosition) * sqrt(min_travel_sq);
          for (Si32 i = 0; i < 200; ++i) {
            SpawnParticle(min_travel_cp,
                g_rocks[min_travel_rock_idx].Velocity +
                          g_directions[Random32(0, (Si32)g_directions.size() - 1)] * (double)Random(0, 6553600ll) / 256.f,
                          Random32(500, 1000) * 3.f / 1000.f);
            SpawnParticle(min_travel_cp,
                          g_rocks[min_travel_rock_idx].Velocity +
                          g_directions[Random32(0, (Si32)g_directions.size() - 1)] * (double)Random(0, 6553600ll) / 65536.f,
                          Random32(500, 1000) * 10.f / 1000.f);
            SpawnParticle(min_travel_cp,
                          g_rocks[min_travel_rock_idx].Velocity +
                          g_directions[Random32(0, (Si32)g_directions.size() - 1)] * (double)Random(0, 6553600ll) / 16386.f,
                          Random32(500, 1000) * 10.f / 1000.f);
          }
          g_boom.Play(2.f);
          SplitRock(min_travel_rock_idx, min_travel_cp);
        }
      }

      if (!g_bullet_idx_to_remove.empty()) {
        std::sort(g_bullet_idx_to_remove.begin(), g_bullet_idx_to_remove.end());
        size_t bullet_idx = g_bullet_idx_to_remove.back();
        g_bullets[bullet_idx] = g_bullets.back();
        g_bullets.pop_back();
        for (Si64 i = (Si64)g_bullet_idx_to_remove.size() - 2; i >= 0; ++i) {
          if (g_bullet_idx_to_remove[(size_t)i] != bullet_idx) {
            bullet_idx = g_bullet_idx_to_remove[(size_t)i];
            g_bullets[bullet_idx] = g_bullets.back();
            g_bullets.pop_back();
          }
        }
        g_bullet_idx_to_remove.clear();
      }
    }
    for (size_t i = 0; i < g_safe_rocks.size(); ++i) {
      Rock &r = g_safe_rocks[i];
      r.Position += r.Velocity * dt;
      r.Angle = CanonicAngle(r.Angle + r.W * dt);
    }
    double t1 = Time();
    total_duration += t1 - t0;
    if (total_duration > 0.01 && total_ticks > 1000.0) {
      tick_duration = total_duration / total_ticks;
     // total_duration = 0.0;
     // total_ticks = 0.0;
    }

    SendMessagesToEarth();

    for (size_t i = 0; i < g_particles.size(); ++i) {
      Particle &p = g_particles[i];
      p.Position += p.Velocity * dt;
    }

    for (size_t i = 0; i < g_particles.size(); ++i) {
      Particle &p = g_particles[i];
      p.TimeToLive -= dt;
      if (p.TimeToLive < 0) {
        p = g_particles.back();
        g_particles.pop_back();
      }
    }
    for (size_t i = 0; i < g_bullets.size(); ++i) {
      Bullet &b = g_bullets[i];
      b.TimeToLive -= dt;
      if (b.TimeToLive < 0) {
        b = g_bullets.back();
        g_bullets.pop_back();
      }
    }

    g_ship.Angle = CanonicAngle(g_ship.Angle);

    UpdateCamera();

    Clear();
    cls();
    g_text_fore_color = Rgba(240, 145, 25);
    print(g_mission_text);


    if (g_rocks.size() == 0) {
      g_text_y = TXT_HEI / 3;
      g_text_x = TXT_WID / 2 - 16;
      print(u8"М И С С И Я   В Ы П О Л Н Е Н А");
    } else if (g_ship.Bullets == 0 && g_bullets.size() == 0) {
      g_text_y = TXT_HEI / 3;
      g_text_x = TXT_WID / 2 - 16;
      print(u8"М И С С И Я   П Р О В А Л Е Н А");
    }

    char buf[1024];
    double speed = Length(g_ship.Velocity);
    double dir = 0.f;
    if (speed > 0.f) {
      Vec2D norm_vel = g_ship.Velocity / speed;
      dir = std::atan2(norm_vel.y, norm_vel.x);
    }
    snprintf(buf, sizeof(buf),  u8"  Скорость: %f м/с\n"
             u8"  Ориентация: %f рад\n"
             u8"  Направление движения: %f рад\n"
             u8"  Угловая скорость: %f рад/с\n"
             u8"  Боезапас: %d\n"
             u8"  Топливо: %f кг\n"
             u8"  Масса: %f кг",
             speed,
             g_ship.Angle,
             dir,
             g_ship.W,
             (int)g_ship.Bullets,
             g_ship.FuelM,
             g_ship.Mass);
    g_text_x = 0;
    g_text_y = TXT_HEI - 8;
    print(buf);

    snprintf(buf, sizeof(buf), u8"Длительность тика: %0.9f c", tick_duration);
    g_text_x = TXT_WID - 35;
    g_text_y = TXT_HEI - 8;
    print(buf);
    snprintf(buf, sizeof(buf), u8"Тиков в секунду: %9.0f", 1.0/tick_duration);
    g_text_x = TXT_WID - 35;
    g_text_y = TXT_HEI - 7;
    print(buf);


    Si64 log_begin = std::max(0ll, (Si64)g_ship.veles_log.size() - 25ll);
    Si64 log_end = g_ship.veles_log.size();
    {
      std::stringstream str;
      for (Si64 i = log_begin; i < log_end; ++i) {
        str << "        " << g_ship.veles_log[i] << "\n";
      }
      g_text_x = 0;
      g_text_y = 3;
      print(str.str().c_str());
    }


    DrawShip();
    DrawBullets();
    DrawParticles();
    DrawRocks();

    DrawTextOverlay();
    ShowFrame();
  }
}

void EasyMain() {
  Initialize();
  //DrawShip();
  g_prev_time = Time();
  PlayTitleScreen();
  if (IsKeyDownward(kKeyEscape)) {
    return;
  }
  g_asteroids_voice.Stop();
  PlayGame();
}
