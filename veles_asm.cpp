// Copyright (c) <2020> Vii
#include "veles_asm.h"

bool BeginsWith(const Ui32* data, const Ui32* prefix) {
  if (!data || !prefix) {
    return (!prefix);
  }
  while (true) {
    if (*prefix == 0) {
      return true;
    }
    if (*data == 0 || *data != *prefix) {
      return false;
    }
    ++data;
    ++prefix;
  }
}

bool BeginsWith(const Ui32* data, String32 &prefix) {
  return BeginsWith(data, prefix.Data());
}

const Ui32* SkipPrefix(const Ui32* data, const Ui32* prefix) {
  const Ui32* begin = data;
  if (!data || !prefix) {
    return data;
  }
  while (true) {
    if (*prefix == 0) {
      return data;
    }
    if (*data == 0 || *data != *prefix) {
      return begin;
    }
    ++data;
    ++prefix;
  }
}

const Ui32* SkipPrefix(const Ui32* data, String32 &prefix) {
  return SkipPrefix(data, prefix.Data());
}


const Ui32* SkipPrefix(const Ui32* data, const Ui32 prefix) {
  if (!data) {
    return data;
  }
  if (prefix == 0) {
    return data;
  }
  if (*data == prefix) {
    return data + 1;
  }
  return data;
}

const Ui32* SkipNonNewline(const Ui32* data) {
  if (data) {
    while (*data != 0 && *data != '\r' && *data != '\n') {
      ++data;
    }
  }
  return data;
}


const Ui32* SkipWhitespaceAndNewline(const Ui32* data) {
  const Ui32* p = data;
  do {
    data = p;
    p = SkipPrefix(p, ' ');
    p = SkipPrefix(p, '\t');
    p = SkipPrefix(p, '\n');
    p = SkipPrefix(p, '\r');
  } while (p != data);
  return p;
}

bool IsEndOrWhitespace(const Ui32* data) {
  return !data || (*data == 0 || *data == ' ' || *data == '\t');
}

bool IsWhitespace(const Ui32* data) {
  return data && (*data == ' ' || *data == '\t');
}

const Ui32* SkipWhitespace(const Ui32* data) {
  const Ui32* p = data;
  while (IsWhitespace(p)) {
    ++p;
  }
  return p;
}

const Ui32* SkipOneNewline(const Ui32* data) {
  const Ui32* p = data;
  p = SkipPrefix(p, '\n');
  if (p != data) {
    p = SkipPrefix(p, '\r');
    return p;
  } 
  p = SkipPrefix(p, '\r');
  if (p != data) {
    p = SkipPrefix(p, '\n');
  } 
  return p;
}

const Ui32* SkipNumber(const Ui32* data, double *out_number = nullptr) {
  if (!data) {
    return data;
  }
  double sign = 1.0;
  double value = 0.0;
  double multiplier = 0.1;
  bool has_digits = false;
  bool has_dot = false;
  const Ui32* p = data;
  const Ui32* p1 = SkipPrefix(p, '-');
  if (p1 != p) {
    sign = -1.0;
  }
  p = p1;
  do {
    p1 = p;
    if (*p == '\0') {
      if (has_digits) {
        if (out_number) {
          *out_number = sign * value;
        }
        return p;
      } else {
        return data;
      }
    }
    if (*p >= '0' && *p <= '9') {
      if (has_dot) {
        value = value + multiplier * static_cast<double>(*p - '0');
        multiplier = multiplier * 0.1;
      } else {
        value = value * 10.0 + static_cast<double>(*p - '0');
      }
      has_digits = true;
      ++p;
    }
    if (*p == '.') {
      if (has_dot) {
        return data;
      } else {
        has_dot = true;
        ++p;
      }
    }
  } while (p != p1);
  if (has_digits) {
    if (out_number) {
      *out_number = sign * value;
    }
    return p;
  } else {
    return data;
  }
}

const Ui32* SkipDecUi64(const Ui32* data, Ui64 *out_number = nullptr) {
  if (!data) {
    return data;
  }
  Ui64 value = 0;
  const Ui32* p = data;
  while (*p >= '0' && *p <= '9') {
    value = value * 10 + static_cast<Ui64>(*p - '0');
    ++p;
  }
  if (p != data) {
    if (out_number) {
      *out_number = value;
    }
    return p;
  }
  return data;
}

const Ui32* SkipHexUi64(const Ui32* data, Ui64 *out_number = nullptr) {
  if (!data) {
    return data;
  }
  Ui64 value = 0;
  const Ui32* p = data;
  while (true) {
    if (*p >= '0' && *p <= '9') {
      value = value * 16 + static_cast<Ui64>(*p - '0');
      ++p;
    } else if (*p >= 'a' && *p <= 'f') {
      value = value * 16 + static_cast<Ui64>(*p - 'a' + 10);
      ++p;
    } else if (*p >= 'A' && *p <= 'F') {
      value = value * 16 + static_cast<Ui64>(*p - 'A' + 10);
      ++p;
    } else {
      break;
    }
  }
  if (p != data && (*p == 'h' || *p == 'H')) {
    p++;
    if (out_number) {
      *out_number = value;
    }
    return p;
  }
  return data;
}

Ui32 Utf32FromUtf8(const char* utf8) {
  Utf32Reader reader;
  reader.Reset(reinterpret_cast<const Ui8*>(utf8));
  return reader.ReadOne();
}

bool IsLetter(Ui32 ch) {
  static const Ui32 ch_a = Utf32FromUtf8(u8"a");
  static const Ui32 ch_z = Utf32FromUtf8(u8"z");
  static const Ui32 ch_A = Utf32FromUtf8(u8"A");
  static const Ui32 ch_Z = Utf32FromUtf8(u8"Z");
  static const Ui32 ch_ra = Utf32FromUtf8(u8"а");
  static const Ui32 ch_rz = Utf32FromUtf8(u8"я");
  static const Ui32 ch_rA = Utf32FromUtf8(u8"А");
  static const Ui32 ch_rZ = Utf32FromUtf8(u8"Я");
  if ((ch >= ch_a && ch <= ch_z) ||
      (ch >= ch_A && ch <= ch_Z) ||
      (ch >= ch_ra && ch <= ch_rz) ||
      (ch >= ch_rA && ch <= ch_rZ)) {
    return true;
  } else {
    return false;
  }
}

bool IsDigit(Ui32 ch) {
  static const Ui32 ch_0 = Utf32FromUtf8(u8"0");
  static const Ui32 ch_9 = Utf32FromUtf8(u8"9");
  if (ch >= ch_0 && ch <= ch_9) {
    return true;
  } else {
    return false;
  }
}

bool IsUnderscore(Ui32 ch) {
  static const Ui32 ch_u = Utf32FromUtf8(u8"_");
  return (ch == ch_u);
}

bool IsMinus(Ui32 ch) {
  static const Ui32 ch_u = Utf32FromUtf8(u8"-");
  return (ch == ch_u);
}

const Ui32* SkipVariableName(const Ui32* data) {
  if (!data) {
    return data;
  }
  bool has_letter = false;
  while (true) {
    if (*data == 0) {
      return data;
    }
    if (IsDigit(*data)) {
      if (!has_letter) {
        return data;
      }
    } else if (IsLetter(*data)) {
      has_letter = true;
    } else if (IsUnderscore(*data)) {
      has_letter = true;
    } else {
      return data;
    }
    ++data;
  }
}

bool CanBeUtf8(const char* begin, const char* end) {
  while (begin != end) {
    if (*begin == '\0') {
      return false;
    }
    ++begin;
  }
  return true;
}

void Veles64Asm::Init() {
  kw_sub = String32(u8"SUB");
  kw_nand = String32(u8"NAND");
  kw_rol = String32(u8"ROL");
  kw_jg = String32(u8"JG");
  kw_fadd = String32(u8"FADD");
  kw_cas = String32(u8"CAS");
  kw_in = String32(u8"IN");
  kw_out = String32(u8"OUT");
  kw_dw = String32(u8"DW");
  kw_sector = String32(u8"SECTOR");
  kw_phys = String32(u8"PHYS");
  kw_virt = String32(u8"VIRT");
  kw_constants = String32(u8"CONSTANTS");

  code.resize(Veles64::kRamSizeWords);
}

void Veles64Asm::SetText(std::vector<Ui8> text8) {
  if (text8.empty()) {
    return;
  }
  String32 text;
  if (CanBeUtf8(reinterpret_cast<const char*>(text8.data()),
        reinterpret_cast<const char*>(text8.data() + text8.size()))) {
    text8.push_back(0);
    text = String32(reinterpret_cast<const char*>(text8.data()));
  } else {
    text = String32(reinterpret_cast<const Ui32*>(text8.data()),
      reinterpret_cast<const Ui32*>(text8.data() + (text8.size()/sizeof(Ui32))*sizeof(Ui32)));
  }

  const Ui32 *data = &text.data[0];
  const Ui32 *p = data;
  while (*p != 0) {
    const Ui32 *p1 = SkipNonNewline(p);
    lines.push_back(String32(p, p1));
    p = SkipOneNewline(p1);
  }
}

void Veles64Asm::OnWord() {
  cur_phys = (cur_phys + 1) & Veles64::kABMask;
  cur_virt = (cur_virt + 1) & Veles64::kABMask;
}

bool IsDec(const Ui32 *p) {
  if (!p || *p == 0) {
    return false;
  }
  p = SkipPrefix(p, '-');
  const Ui32 *p2 = p;
  while (*p2 >= '0' && *p2 <= '9') {
    ++p2;
  }
  if (p2 == p) {
    return false;
  }
  if (IsEndOrWhitespace(p2)) {
    return true;
  }
  if (*p2 == ';') {
    return true;
  }
  return false;
}

bool IsHex(const Ui32 *p) {
  if (!p || *p == 0) {
    return false;
  }
  const Ui32 *p2 = p;
  while ((*p2 >= '0' && *p2 <= '9')
      || (*p2 >= 'a' && *p2 <= 'f')
      || (*p2 >= 'A' && *p2 <= 'F')) {
    ++p2;
  }
  if (p2 == p) {
    return false;
  }
  const Ui32 *p4 = SkipPrefix(p2, 'h');
  if (p4 == p2) {
    p4 = SkipPrefix(p2, 'H');
  }
  if (p4 == p2) {
    return false;
  }
  if (IsEndOrWhitespace(p4)) {
    return true;
  }
  if (*p4 == ';') {
    return true;
  }
  return false;
}

const Ui32* Veles64Asm::ParseInteger(Si64 line_id, const Ui32 *p, Ui64 *out_value) {
  if (!p) {
    return p;
  }
  Ui64 value = 0;
  bool is_positive = true;
  // Format options:
  // 11h chars: abcdefABCDEF0123456789hH
  // 22  chars: 0123456789
  // -33 chars: -0123456789

  if (IsHex(p)) {
    // it is 11h
    const Ui32 *p2 = SkipHexUi64(p, &value);
    if (p2 == p) {
      first_error.Set(line_id, -1, "Error parsing hex value");
      return p;
    }
    if (out_value) {
      *out_value = value;
    }
    return p2;
  } else {
    // it must be 22 or -33
    const Ui32 *p2 = SkipPrefix(p, '-');
    if (p2 != p) {
      is_positive = false;
    }
    p = p2;
    p2 = SkipDecUi64(p, &value);
    if (p2 == p) {
      if (!is_positive) {
        first_error.Set(line_id, -1, "Error parsing negative dec value");
      }
      return p;
    }
    if (is_positive) {
      if (value > 0x7fffffffffffffffull) {
        first_error.Set(line_id, -1, "Value is too large for a signed 64 bit integer");
        return p;
      }
      if (out_value) {
        *out_value = value;
      }
      return p2;
    }
    if (value <= 0x7fffffffffffffffull) {
      if (out_value) {
        *out_value = static_cast<Ui64>(-1 * static_cast<Si64>(value));
      }
      return p2;
    }
    if (value == 0x8000000000000000ull) {
      if (out_value) {
        *out_value = value;
      }
      return p2;
    }
    first_error.Set(line_id, -1, "Value is too small for a signed 64 bit integer");
    return p;
  }
}

const Ui32* Veles64Asm::ParseArgument(Si64 line_id, const Ui32 *p, Arg *out_arg) {
  if (!p) {
    first_error.Set(line_id, -1, "Missing argument");
    return p;
  }
  if (!out_arg) {
    first_error.Set(line_id, -1, "Internal error while parsing argument");
    return p;
  }
  if (IsHex(p) || IsDec(p)) {
    Ui64 value = 0;
    const Ui32* p2 = ParseInteger(line_id, p, &value);
    if (p2 != p) {
      out_arg->type = kArgConst;
      Si64 id = -1;
      auto it = const_to_id.find(value);
      if (it == const_to_id.end()) {
        id = const_dict.size();
        const_dict.push_back(value);
        const_to_id[value] = id;
      } else {
        id = it->second;
      }
      out_arg->const_id = id;
      return p2;
    } else {
      first_error.Set(line_id, -1, "Internal error while parsing const argument");
      return p;
    }
  }
  if (*p == '"') {
    const Ui32 *p2 = p + 1;
    while (*p2 != 0 && *p2 != '"') {
      p2++;
    }
    if (*p2 == 0) {
      first_error.Set(line_id, -1, "Missing message argument closing quotation mark");
      return p;
    }
    p2++;
    out_arg->type = kArgMessage;
    String32 message(p + 1, p2 - 1);
    Si64 message_id = message_dict.size();
    message_dict.push_back(message);

    Si64 id = -1;
    auto it = const_to_id.find(message_id);
    if (it == const_to_id.end()) {
      id = const_dict.size();
      const_dict.push_back(message_id);
      const_to_id[message_id] = id;
    } else {
      id = it->second;
    }
    out_arg->message_id = id;
    return p2;
  } else if (*p == '&') {
    ++p;
    const Ui32 *p2 = SkipVariableName(p);
    if (p2 == p) {
      first_error.Set(line_id, -1, "Missing label after & (address of) argument type specifier");
      return p;
    }
    out_arg->type = kArgLabelOffset;
    out_arg->label = String32(p, p2);
    return p2;
  } else {
    const Ui32 *p2 = SkipVariableName(p);
    if (p2 == p) {
      first_error.Set(line_id, -1, "Unexpected token in place of an argument");
      return p;
    }
    out_arg->type = kArgLabel;
    out_arg->label = String32(p, p2);
    return p2;
  }
}

const Ui32* Veles64Asm::ParseKeyword(const Ui32 *p, Si64 line_id) {
  Word *word = &code[cur_phys];
  const Ui32 *p2 = p;
  p2 = SkipVariableName(p);
  if (p2 == p) {
    return p;
  }
  String32 keyword(p, p2);
  if (keyword == kw_sub
      || keyword == kw_nand
      || keyword == kw_rol
      || keyword == kw_jg
      || keyword == kw_fadd
      || keyword == kw_cas
      || keyword == kw_in
      || keyword == kw_out) {
    const Ui32 *p3 = SkipWhitespace(p2);
    if (p3 == p2) {
      first_error.Set(line_id, -1, "Missing whitespace after keyword");
      return p;
    }
    const Ui32 *p4 = ParseArgument(line_id, p3, &word->arg_a);
    p4 = SkipWhitespace(p4);
    p4 = ParseArgument(line_id, p4, &word->arg_b);
    p4 = SkipWhitespace(p4);
    p4 = ParseArgument(line_id, p4, &word->arg_c);
    if (first_error.is_set) {
      return p;
    }
    if (word->arg_a.type == kArgUndefined ||
        word->arg_b.type == kArgUndefined ||
        word->arg_c.type == kArgUndefined) {
      first_error.Set(line_id, -1, "Missing arguments after keyword, 3 arguments expected");
      return p;
    }
    if (word->type != kWordUndefined) {
      first_error.Set(line_id, word->line_id, "Instructions collide in the same physical memory cell");
      return p;
    }
    if (keyword == kw_in || keyword == kw_out) {

    }
    word->line_id = line_id;
    word->type = kWordInstruction;
    word->sector_id = Si64(sector_dict.size()) - 1;

    if (keyword == kw_sub) {
      word->opcode = Veles64::kOpSub;
      if (word->arg_a.type == kArgConst) {
        first_error.Set(line_id, -1, "The first argument (the result destination) of SUB can not be a constant");
        return p;
      }
    } else if (keyword == kw_nand) {
      if (word->arg_a.type == kArgConst) {
        first_error.Set(line_id, -1, "The first argument (the result destination) of NAND can not be a constant");
        return p;
      }
      word->opcode = Veles64::kOpNand;
    } else if (keyword == kw_rol) {
      if (word->arg_a.type == kArgConst) {
        first_error.Set(line_id, -1, "The first argument (the result destination) of ROL can not be a constant");
        return p;
      }
      word->opcode = Veles64::kOpRol;
    } else if (keyword == kw_jg) {
      word->opcode = Veles64::kOpJg;
    } else if (keyword == kw_fadd) {
      if (word->arg_a.type == kArgConst) {
        first_error.Set(line_id, -1, "The first argument (the fetch destination) of FADD can not be a constant");
        return p;
      }
      if (word->arg_b.type == kArgConst) {
        first_error.Set(line_id, -1, "The second argument (the incremented value) of FADD can not be a constant");
        return p;
      }
      word->opcode = Veles64::kOpFadd;
    } else if (keyword == kw_cas) {
      if (word->arg_a.type == kArgConst) {
        first_error.Set(line_id, -1, "The first argument (the compared/swapped value) of CAS can not be a constant");
        return p;
      }
      if (word->arg_c.type == kArgConst) {
        first_error.Set(line_id, -1, "The third argument (the new/old value) of CAS can not be a constant");
        return p;
      }
      word->opcode = Veles64::kOpCas;
    } else if (keyword == kw_in) {
      if (word->arg_a.type == kArgConst) {
        first_error.Set(line_id, -1, "The first argument (the counter) of IN can not be a constant");
        return p;
      }
      word->opcode = Veles64::kOpIn;
    } else if (keyword == kw_out) {
      if (word->arg_a.type == kArgConst) {
        first_error.Set(line_id, -1, "The first argument (the counter) of OUT can not be a constant");
        return p;
      }
      word->opcode = Veles64::kOpOut;
    } else {
      first_error.Set(line_id, -1, "Internal error while parsing keyword");
      return p;
    }
    OnWord();
    p2 = p4;
  } else if (keyword == kw_dw) {
    Si64 count = 0;
    while (true) {
      if (*p2 == 0) {
        if (count > 0) { 
          return p2;
        }
        first_error.Set(line_id, -1, "Missing value after 'DW'");
        return p;
      }
      const Ui32 *p3 = SkipWhitespace(p2);
      if (p3 == p2) {
        first_error.Set(line_id, -1, "Missing whitespace after 'DW'");
        return p;
      }
      const Ui32 *pc = SkipPrefix(p3, ';');
      if (pc != p3) {
        if (count > 0) { 
          return p2;
        }
        first_error.Set(line_id, -1, "Missing value after 'DW'");
        return p;
      }
      const Ui32 *p4 = p3;
      if (*p3 == '"') {
        p4 = p3 + 1;
        while (*p4 != 0 && *p4 != '"') {
          p4++;
        }
        if (*p4 == 0) {
          first_error.Set(line_id, -1, "Missing message DW closing quotation mark");
          return p;
        }
        p4++;
        String32 message(p3 + 1, p4 - 1);
        Si64 message_id = message_dict.size();
        message_dict.push_back(message);
        if (word->type != kWordUndefined) {
          first_error.Set(line_id, word->line_id, "Instructions collide in the same physical memory cell");
          return p;
        }
        word->line_id = line_id;
        word->type = kWordData;
        word->sector_id = Si64(sector_dict.size()) - 1;
        word->dw.type = kDwValue;
        word->dw.value = message_id;
        OnWord();
        word = &code[cur_phys];
        p2 = p4;
        count++;
      } else if (*p3 == '?') {
        p4 = p3 + 1;
        if (word->type != kWordUndefined) {
          first_error.Set(line_id, word->line_id, "Instructions collide in the same physical memory cell");
          return p;
        }
        word->line_id = line_id;
        word->type = kWordData;
        word->sector_id = Si64(sector_dict.size()) - 1;
        word->dw.type = kDwValue;
        word->dw.value = 0;
        OnWord();
        word = &code[cur_phys];
        p2 = p4;
        count++;
      } else {
        Ui64 value = 0;
        p4 = ParseInteger(line_id, p3, &value);
        if (first_error.is_set) {
          return p;
        }
        if (p4 != p3) {
          if (word->type != kWordUndefined) {
            first_error.Set(line_id, word->line_id, "Instructions collide in the same physical memory cell");
            return p;
          }
          word->line_id = line_id;
          word->type = kWordData;
          word->sector_id = Si64(sector_dict.size()) - 1;
          word->dw.type = kDwValue;
          word->dw.value = value;
          OnWord();
          word = &code[cur_phys];
          p2 = p4;
          count++;
        } else {
          p4 = SkipVariableName(p3);
          if (p4 == p3) {
            first_error.Set(line_id, -1, "Unexpected token after 'DW'");
            return p;
          }
          if (word->type != kWordUndefined) {
            first_error.Set(line_id, word->line_id, "Instructions collide in the same physical memory cell");
            return p;
          }
          word->line_id = line_id;
          word->type = kWordData;
          word->sector_id = Si64(sector_dict.size()) - 1;
          word->dw.type = kDwLabel;
          word->dw.label = String32(p3, p4);
          OnWord();
          word = &code[cur_phys];
          p2 = p4;
          count++;
        }
      }
    }

  } else if (keyword == kw_sector) {
    const Ui32 *p3 = SkipWhitespace(p2);
    if (p3 == p2) {
      first_error.Set(line_id, -1, "Missing whitespace after 'SECTOR'");
      return p;
    }
    const Ui32 *p4 = SkipVariableName(p3);
    if (p4 == p3) {
      first_error.Set(line_id, -1, "Missing whitespace after 'SECTOR'");
      return p;
    }
    String32 name(p3, p4);
    for (size_t i = 0; i < sector_dict.size(); ++i) {
      if (name == sector_dict[i]) {
        first_error.Set(line_id, -1, "Sector name is already in use");
        return p;
      }
    }
    sector_dict.push_back(name);
    p2 = p4;
  } else if (keyword == kw_phys || keyword == kw_virt) {
    const Ui32 *p3 = SkipWhitespace(p2);
    if (p3 == p2) {
      first_error.Set(line_id, -1, "Missing whitespace after 'PHYS' or 'VIRT'");
      return p;
    }
    Ui64 value = 0;
    const Ui32 *p4 = ParseInteger(line_id, p3, &value);
    if (p4 == p3) {
      first_error.Set(line_id, -1, "Missing an address after 'PHYS' or 'VIRT'");
      return p;
    }
    if (keyword == kw_phys) {
      cur_phys = value;
    } else {
      cur_virt = value;
    }
    p2 = p4;
  } else if (keyword == kw_constants) {
    const_phys_begin = cur_phys;
    const_virt_begin = cur_virt;
  } else {
    // not a keyword
    return p;
  }
  
  const Ui32 *ws = SkipWhitespace(p2);
  // a comment?
  const Ui32 *pc = SkipPrefix(ws, ';');
  if (pc != ws) {
    // a comment
    return p2;
  } else {
    // EOL?
    if (*pc != 0) {
      first_error.Set(line_id, -1, "Unexpected token after instruction arguments");
      return p;
    }
    return p2;
  }
}

void Veles64Asm::Parse() {
  for (Si64 line_id = 0; line_id < lines.size(); ++line_id) {
    const Ui32 *p = &lines[line_id].data[0];
    p = SkipWhitespace(p);
    const Ui32 *p2 = ParseKeyword(p, line_id);
    if (first_error.is_set) {
      return;
    }
    if (p2 == p) {
      // not keyword, a label?
      p2 = SkipVariableName(p);
      if (p2 != p) {
        // a label?
        const Ui32 *p3 = SkipWhitespace(p2);
        const Ui32 *p4 = SkipPrefix(p3, ':');
        if (p4 != p3) {
          // a label.
          String32 label(p, p2);
          auto it = labels.find(label);
          if (it == labels.end()) {
            labels[label] = LabelInfo(cur_virt, cur_phys, line_id);
            p = SkipWhitespace(p4);
            p2 = ParseKeyword(p, line_id);
            if (first_error.is_set) {
              return;
            }
            if (p2 == p) {
              // not a keyword, a comment?
              p2 = SkipPrefix(p, ';');
              if (p2 != p) {
                // a comment
                continue;
              } else {
                // EOL?
                if (*p != 0) {
                  first_error.Set(line_id, -1, "Unexpected token after ':'");
                  return;
                }
                continue;
              }
            }
            continue;
          } else {
            first_error.Set(line_id, it->second.line_id, "Duplicate label");
            return;
          }
        } else {
            first_error.Set(line_id, -1, "No ':' after label at the beginning");
            return;
        }
      }
      // not label, a comment?
      p2 = SkipPrefix(p, ';');
      if (p2 != p) {
        // a comment
        continue;
      } else {
        if (*p != 0) {
            first_error.Set(line_id, -1, "Unexpected line beginning");
            return;
        }
        continue;
      }
    }
  }
}

void Veles64Asm::ProduceConstants() {
  if (first_error.is_set) {
    return;
  }
  if (const_phys_begin == -1 && !const_dict.empty()) {
    first_error.Set(-1, -1, "Missing CONSTANTS directive");
    return;
  }
  cur_phys = const_phys_begin;
  cur_virt = const_virt_begin;
  for (Si64 i = 0; i < const_dict.size(); ++i) {
    Word *word = &code[cur_phys];
    if (word->type != kWordUndefined) {
      first_error.Set(word->line_id, -1, "Instruction is overwritten with autogenerated CONSTANTS");
      return;
    }
    word->line_id = -1;
    word->type = kWordData;
    word->sector_id = -1;
    word->dw.type = kDwValue;
    word->dw.value = const_dict[i];
    OnWord();
  }
}

void Veles64Asm::ProduceOffsetConstantForArg(Arg *arg, Si64 line_id) {
  if (arg->type == kArgLabelOffset) {
    // Find the label
    auto it = labels.find(arg->label);
    if (it == labels.end()) {
      first_error.Set(line_id, -1, "Label argument not found, can not take offset");
      return;
    }
    Ui64 value = it->second.virt;

    // Produce the const
    arg->type = kArgConst;
    auto it2 = const_to_id.find(value);
    if (it2 == const_to_id.end()) {
      arg->const_id = const_dict.size();
      const_dict.push_back(value);
      const_to_id[value] = arg->const_id;
    } else {
      arg->const_id = it2->second;
    }
  }
}

Ui64 Veles64Asm::ResolveArg(const Arg& arg, Si64 line_id) {
  switch (arg.type) {
    case kArgUndefined:
      first_error.Set(line_id, -1, "Undefined argument");
      return 0;
    case kArgLabel:
      {
        auto it = labels.find(arg.label);
        if (it == labels.end()) {
          first_error.Set(line_id, -1, "Label argument not found");
          return 0;
        }
        return it->second.virt;
      }
    case kArgConst:
      return ((const_virt_begin + arg.const_id) & Veles64::kABMask);
    case kArgMessage:
      return ((const_virt_begin + arg.message_id) & Veles64::kABMask);
  }
  first_error.Set(line_id, -1, "Error resolving argument");
  return 0;
}

void Veles64Asm::InjectOffsetConstants() {
  if (first_error.is_set) {
    return;
  }
  for (Si64 i = 0; i < code.size(); ++i) {
    Word *word = &code[i];
    if (word->type == kWordInstruction) {
      ProduceOffsetConstantForArg(&word->arg_a, word->line_id);
      ProduceOffsetConstantForArg(&word->arg_b, word->line_id);
      ProduceOffsetConstantForArg(&word->arg_c, word->line_id);
    }
  }
}

void Veles64Asm::Generate() {
  if (first_error.is_set) {
    return;
  }
  compiled.resize(code.size());
  for (Si64 i = 0; i < code.size(); ++i) {
    Word *word = &code[i];
    switch (word->type) {
      case kWordUndefined:
        compiled[i] = 0;
        break;
      case kWordInstruction:
        {
          Ui64 a = ResolveArg(word->arg_a, word->line_id);
          Ui64 b = ResolveArg(word->arg_b, word->line_id);
          Ui64 c = ResolveArg(word->arg_c, word->line_id);
          compiled[i] = MakeOp(word->opcode, a, b, c);
        }
        break;
      case kWordData:
        switch (word->dw.type) {
          case kDwUndefined:
            compiled[i] = 0;
            break;
          case kDwValue:
            compiled[i] = word->dw.value;
            break;
          case kDwLabel:
            {
              auto it = labels.find(word->dw.label);
              if (it == labels.end()) {
                first_error.Set(word->line_id, -1, "Label not found");
                return;
              }
              compiled[i] = it->second.virt;
            }
            break;
        }
        break;
    }
  }
  return;
}

void Veles64Asm::Compile(std::vector<Ui8> text8) {
  Init();
  SetText(text8);
  Parse();
  InjectOffsetConstants();
  ProduceConstants();
  Generate();
}
