// Copyright (c) <2019> Vii
#include "veles.h"

#define RAM(x) (ram[(ram[((x)>>(21-4))+(cpu_id<<4)]+((x)&((1ull<<(21-4))-1)))&Veles64::kABMask])

void Step(Veles64* v) {
  v->Time++;
  Ui64 *ram = v->Ram;
  for (Si64 cpu_id = 0; cpu_id < Veles64::kCoreCount; ++cpu_id) {
    Veles64::CoreState &core = v->Core[cpu_id];
    const Ui64 instr = RAM(core.ip);
    const Ui64 c = instr >> Veles64::kCShift;
    core.c1 = RAM(c);
  }
  for (Si64 cpu_id = 0; cpu_id < Veles64::kCoreCount; ++cpu_id) {
    Veles64::CoreState &core = v->Core[cpu_id];
    const Ui64 instr = RAM(core.ip);

    const Ui64 op = instr & Veles64::kOpcodeMask;
    const Ui64 a = (instr >> Veles64::kAShift) & Veles64::kABMask;
    const Ui64 b = (instr >> Veles64::kBShift) & Veles64::kABMask;

    switch (op) {
      case Veles64::kOpSub:
        RAM(a) = RAM(b) - core.c1;
        core.ip = (core.ip + 1) & Veles64::kABMask;
        break;
      case Veles64::kOpNand:
        RAM(a) = ~(RAM(b) & core.c1);
        core.ip = (core.ip + 1) & Veles64::kABMask;
        break;
      case Veles64::kOpRol:
        {
          Ui64 shift = core.c1 & 63;
          Ui64 b2 = RAM(b);
          RAM(a) = shift ? ((b2 << shift) | (b2 >> (64 - shift))) : b2;
          core.ip = (core.ip + 1) & Veles64::kABMask;
        }
        break;
      case Veles64::kOpJg:
        if (static_cast<Si64>(RAM(b)) > static_cast<Si64>(core.c1)) {
          core.ip = (a & Veles64::kABMask);
        } else {
          core.ip = (core.ip + 1) & Veles64::kABMask;
        }
        break;
      case Veles64::kOpFadd: // ATOMIC FETCH ADD read c as C1; {atomic a = b; b = b + C1;}
        {
          Ui64 *pb = &RAM(b);
          RAM(a) = *pb;
          *pb += core.c1;
          core.ip = (core.ip + 1) & Veles64::kABMask;
        }
        break;
      case Veles64::kOpCas: // CAS a b c: read c as C1; {atomic compare a == b and if so c=a; a = C1}
        {
          Ui64 *pa = &RAM(a);
          if (*pa == RAM(b)) {
            Ui64 c = instr >> Veles64::kCShift;
            RAM(c) = *pa;
            *pa = core.c1;
          }
          core.ip = (core.ip + 1) & Veles64::kABMask;
        }
        break;
      case Veles64::kOpIn: // in from port с to addr b size a; a=0; STALLS
        {
          Ui64 to_copy = RAM(a) & 255;
          if (to_copy) {
            to_copy--;
            Ui64 *pa = &RAM(a);
            Ui64 dst = (b + to_copy) & Veles64::kABMask;
            RAM(dst) = v->OpInput(v, core.c1);
            *pa = to_copy;
          } else {
            core.ip = (core.ip + 1) & Veles64::kABMask;
          }
        }
        break;
      case Veles64::kOpOut: // out from addr b to port c size a; a=0; STALLS
        {
          Ui64 *pa = &RAM(a);
          Ui64 to_copy = (*pa) & 255;
          if (to_copy) {
            to_copy--;
            Ui64 src = (b + to_copy) & Veles64::kABMask;
            v->OpOutput(v, RAM(src), core.c1);
            *pa = to_copy;
          } else {
            core.ip = (core.ip + 1) & Veles64::kABMask;
          }
        }
        break;
    };
  }
}

Ui64 MakeOp(Veles64::Opcode opcode, Ui64 a, Ui64 b, Ui64 c) {
  return (static_cast<Ui64>(opcode) & Veles64::kOpcodeMask) | 
      ((a & Veles64::kABMask) << Veles64::kAShift) |
      ((b & Veles64::kABMask) << Veles64::kBShift) |
      (c << Veles64::kCShift);
}


