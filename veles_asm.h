#pragma once

#include <arctic/engine/easy.h>
#include "veles.h"
#include "string32.h"

#include <unordered_map>
#include <deque>

using namespace arctic;


struct Veles64Asm {
  struct LabelInfo {
    Si64 virt = -1;
    Si64 phys = -1;
    Si32 line_id = -1;

    LabelInfo() {
    };

    LabelInfo(Si64 virt_, Si64 phys_, Si64 line_id_)
      : virt(virt_)
      , phys(phys_)
      , line_id(static_cast<Si32>(line_id_)) {
    }  
  };
  std::deque<String32> message_dict;
  std::unordered_map<String32, LabelInfo> labels;
  std::deque<Ui64> const_dict;
  std::unordered_map<Ui64, Si64> const_to_id;
  std::deque<String32> sector_dict;
  Si64 const_phys_begin = -1;
  Si64 const_virt_begin = -1;

  enum WordType {
    kWordUndefined = 0,
    kWordInstruction = 1,
    kWordData = 2
  };
  enum ArgType {
    kArgUndefined = 0,
    kArgLabel = 1,
    kArgLabelOffset = 2,
    kArgConst = 3,
    kArgMessage = 4
  };
  struct Arg {
    ArgType type = kArgUndefined;
    String32 label;
    Si64 const_id = -1;
    Si64 message_id = -1;
  };
  enum DwType {
    kDwUndefined = 0,
    kDwValue = 1,
    kDwLabel = 3
  };
  struct Dw {
    DwType type = kDwUndefined;
    Ui64 value = 0;
    String32 label;
  };
  struct Word {
    Si64 line_id = -1;
    WordType type = kWordUndefined;
    Si64 sector_id = -1;
    Dw dw;
    Veles64::Opcode opcode = Veles64::kOpSub;
    Arg arg_a;
    Arg arg_b;
    Arg arg_c;
  };
  struct Error {
    bool is_set = false;
    Si64 line_id = -1;
    Si64 conflicting_line_id = -1;
    std::string description;

    void Set(Si64 line_id_, Si64 conflicting_line_id_, std::string description_) {
      if (!is_set) {
        is_set = true;
        line_id = line_id_;
        conflicting_line_id = conflicting_line_id_;
        description = description_;
      }
    }
  };

  Error first_error;
  std::vector<Word> code;
  std::vector<Ui64> compiled;
  Ui64 cur_phys = 0;
  Ui64 cur_virt = 0;

  std::deque<String32> lines;

  String32 kw_sub;
  String32 kw_nand;
  String32 kw_rol;
  String32 kw_jg;
  String32 kw_fadd;
  String32 kw_cas;
  String32 kw_in;
  String32 kw_out;
  String32 kw_dw;
  String32 kw_sector;
  String32 kw_phys;
  String32 kw_virt;
  String32 kw_constants;

  void Compile(std::vector<Ui8> text8);

  void Init();
  void SetText(std::vector<Ui8> text8);
  void Parse();
  void InjectOffsetConstants();
  void ProduceConstants();
  void Generate();

  void OnWord();
  const Ui32* ParseInteger(Si64 line_id, const Ui32 *p, Ui64 *out_value);
  const Ui32* ParseArgument(Si64 line_id, const Ui32 *p, Arg *out_arg);
  const Ui32* ParseKeyword(const Ui32 *p, Si64 line_id);
  const Ui32* ParseKeyword(const Ui32 *p);
  void ProduceOffsetConstantForArg(Arg *arg, Si64 line_id);
  Ui64 ResolveArg(const Arg& arg, Si64 line_id);
};

